CC = gcc
SPEFLAG = 
DEBUG=yes
ifeq ($(OS), Windows_NT)
	EXEC = mido-age.exe
	SPEFLAG = -lmingw32 -ISDL\include -LSDL\lib
else
	EXEC= mido-age
endif

ifeq ($(DEBUG),yes)
	CFLAGS = -g -lm -iquote include -iquote Structure $(SPEFLAG) -lSDL2main -lSDL2 -lSDL2_ttf -lSDL2_image -Wall
else
	CFLAGS = -Wall -lSDLmain -lSDL 
endif
DIR =  ./ ./Prompt ./Structure ./Structure/Lists ./Structure/Functions_Characters ./Structure/Functions_Characters/* ./Saves ./Graphics ./MoveManage ./Execution
LIB = $(wildcard include/*.h) 
SRC = $(foreach dir, $(DIR), $(wildcard $(dir)/*.c))
OBJDIR = ./objet
OBJ = $(subst $(DIR),,$(SRC))
OBJET = $(subst .c,.o,$(OBJ))
all: $(OBJDIR) $(EXEC)
	@echo Compiling $(EXEC)...

$(OBJDIR):
	mkdir $(OBJDIR)

$(EXEC):$(OBJ)
	$(CC) -o $(EXEC) $^ $(CFLAGS) 

$(OBJET): $(SRC)
	@echo $(OBJET)
	$(CC) $(CFLAGS) -c $< $@


.PHONY: clean mrproper

clean:
	@echo Deleting object $(OBJ)...
	rm -rf $(wildcard $(OBJDIR)/*.o)

mrproper:
	@echo Deleting $(EXEC)...
	rm -rf $(EXEC)
