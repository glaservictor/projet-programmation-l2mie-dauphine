#include<stdlib.h>
#include<stdio.h>

#ifndef CASTLE_H
#include"castleWorld.h"
#endif 

#ifndef LIST_CASTLE_H
#define LIST_CASTLE_H

/**
 * @brief Cette structure permet de représenter la liste des chateaux possésés par
 * un joueur, et donc les chateaux d'une couleur
 * 
 */

typedef struct ListDChainedCastle
{
    struct VertexCastle *head;
    struct VertexCastle *tail;
    int size;
}ListDChainedCastle;


/**
 * @brief Maillon permettant des regrouper des chateaux dans une liste
 * 
 */

typedef struct VertexCastle
{
    struct Castle *castle;
    struct VertexCastle *next;
    struct VertexCastle *prev;
}VertexCastle;

#endif

/**
 * @brief Affiche une liste de chateaux
 * 
 * @param list 
 */

void DisplayListCastle(ListDChainedCastle *list);

/**
 * @brief Permet d'insérer un chateau (son maillon) dans une liste de chateaux à un certain indice donné
 * 
 * @param list 
 * @param vertex 
 * @param n , l'indice auquel on insère le maillon
 * @return ListDChainedCastle* 
 */

ListDChainedCastle* AddListCastle(ListDChainedCastle* list, VertexCastle* vertex, int n);

/**
 * @brief Supprime de la liste de chateaux le maillon d'un chateau d'indice n
 * 
 * @param list 
 * @param n 
 * @return VertexCastle* , le maillon supprimé de la liste
 */

VertexCastle* DeleteListCastle(ListDChainedCastle* list, int n);

/**
 * @brief Permet d'ajouter un maillon en tête d'une liste de chateaux
 * 
 * @param list 
 * @param vertex 
 */

void AddHeadCastle(ListDChainedCastle *list, VertexCastle *vertex);

/**
 * @brief Permet d'ajouter un maillon en queue d'une liste de chateaux
 * 
 * @param list 
 * @param vertex 
 */

void AddTailCastle(ListDChainedCastle* list, VertexCastle* vertex);

/**
 * @brief Permet d'initialiser une maillon de chateau
 * 
 * @param c 
 * @return VertexCastle* 
 */

VertexCastle* VertexInitCastle(Castle *c);

/**
 * @brief Permet d'initialiser une liste de chateaux
 * 
 * @return ListDChainedCastle* 
 */

ListDChainedCastle* ListeInitialisationChateau();
