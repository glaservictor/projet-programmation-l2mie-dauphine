#include<stdio.h>
#include"chosePath.h"

#ifndef DIJ_H
#define DIJ_H

/**
 * @brief Methode de détéction du plus court chemin sur un graphe donné O(nlog(n))
 * @param Un graph ( pair *)
 * @param La taille du graphe associé 
 * 
 * @return int* ( la fonction père du graphe )
 */
int *dijkstra(const Pair*,int,size_t);

#endif