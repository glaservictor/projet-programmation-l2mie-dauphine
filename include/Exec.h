#include<stdio.h>
#include<stdlib.h>
#include"World.h"
#include"Coord.h"
#include"DisplayPrompt.h"
#include"castleWorld.h"
#include"ListChainedHeritageCastle.h"
#include "chosePath.h"
#include "moveFin.h"
#include"freedom.h"

 #ifndef EXEC_H
 #define EXEC_H


/**
 * @brief Permet l'initialisation de la partie, O(i*j)
 * avec le nombre de lignes du plateau et j le nombre de colonnes
 * 
 * @return World* 
 */

World* InitialisationPartie(int);

/**
 * @brief Cette fonction permet la transformation d'un Seigneur en chateau, 
 * O(n+p+q) avec n le nombre de chateaux de la couleur,
 * p le nombre de personnages de la liste d'élements du château d'origine auquel était associé au seigneur,
 * q le nombre de personnages de la liste de voisins
 * 
 * @param world 
 * @param lord 
 * @param color 
 * @return int 
 * 0 si la transformation a pu se faire
 * 1 sinon (il y avait déjà un chateau sur la case où était le seigneur)
 */

int LordToCastle(World* world, VertexArmy* lord, char color);

/**
 * @brief Cette fonction ajoute un personnage à un chateau, O(1)
 * 
 * @param color 
 * @param world 
 * @param type 
 * @param coord 
 * @param dest 
 * @param castle 
 */

void AddChar(char color, World* world, Type type,Coordinates coord,Coordinates dest,Castle *castle);

/**
 * @brief Cette fonction permet de créer le personnage produit par un chateau, O(1)
 * 
 * @param castle 
 * @param world 
 */

void CastleBear(Castle* castle, World* world);

/**
 * @brief Cette fonction vérifie s'il reste un chateau à jouer, c'est à dire un chateau dont la tache est à 0,
 * O(n) avec n le nombre de chateaux de la couleur
 * 
 * @param list 
 * @return VertexCastle*, le maillon d'un chateau n'ayant pas joué
 */

VertexCastle *RemainCastlePlaying(ListDChainedCastle *list);

/**
 * @brief Cette fonction vérifie s'il reste un personnage à jouer, on considère un chateau dont la liste est déjà triée (voir plus tard)
 * O(p) avec p le nombre de personnages de la couleur
 * 
 * @param castle 
 * @param typ 
 * @return VertexArmy*, un personnage n'ayant pas joué 
 */

VertexArmy* RemainCharPlaying(Castle* castle, int typ);

/**
 * @brief Cette fonction retourne le type de personnage à jouer (si on avait un affichage console l'ordre aurait été manants puis 
 * guerriers puis seigneurs), si tous les personnages ont déjà, on met en oeuvre la fin de tour (fonction précédente),
 * S'il reste un personnage à jouer : O(n) avec n le nombre de personnages de la couleur,
 * Sinon O(t) avec t le nombre total d'unités de la partie
 * 
 * @param world 
 * @return int 
 * 2 si il reste un manant à jouer
 * 1 s'il reste un guerrier à jouer
 * 0 s'il reste un seigneur à jouer
 * -1 si le tour est fini
 */

int typeOfCharPlaying(World* world);

/**
 * @brief Cette fonction donne un ordre à un personnage
 * O(1)
 * 
 * @param character 
 * @param order 
 */

void AddOrder(VertexArmy* character, char order);

/**
 * @brief Cette fonction implémente le déplacement d'un personnage
 * La complexité dépend de si le déplacement donne lieu à des combats
 * 
 * 
 * @param world 
 * @param army 
 * @param dest 
 * @return int 
 * 
 * -1 si le déplacement n'a pas été possible
 * 0 si le déplacement a donné lieu à un combat
 * 1 si le déplacement a été possible, sans combat
 * -2 si le déplacement a donné lieu à un combat ayant achevé la partie
 * 
 */

int Mooving(World* world, Characters* character,Coordinates,char choose);

/**
 * @brief Cette fonction renvoie, pour le personnage considéré, le chateau qui lui est associé, c'est à dire soit le chateau initial
 * si le personnage est présent depuis l'initialisation de la partie, soit le chateau l'ayant produit.
 * O(n) avec n le nombre d'unités de la couleur du personnage
 * 
 * @param choose
 * @param character 
 * @param world 
 * @return VertexCastle* , le chateau associé au personnage
 */

VertexCastle* FindCastleOfChar(VertexArmy* character, World* world);

/**
 * @brief Cette fonction renvoie, pour une liste de personnages, et un des personnages de cette liste, l'indice du personnage dans cette
 * liste. Si n est le nombre de personnages de la liste, les indices vont de 0 à n-1,
 * O(n) avec n le nombre de personnages de la liste
 * 
 * 
 * @param list 
 * @param character 
 * @return int , l'indice du personnage
 */

int FindOutIndexChar(ListDChainedArmy *list, VertexArmy *character);

/**
 * @brief Cette fonction réalise le combat entre 2 personnages, supprimant le perdant et renvoyant le vainqueur,
 * O(n+p) avec n le nombre d'unités de la couleur du personnage perdant, et p le nombre de personnages de la liste de voisins
 * 
 * @param world 
 * @param character1 
 * @param character2 
 * @return VertexArmy* , le vainqueur du combat
 */

VertexArmy *FightChar(World *world, VertexArmy *character1, VertexArmy *character2, char choose);

/**
 * @brief Cette fonction permet le suicide d'un personnage
 * O(n+p) avec n le nombre d'unités de la couleur, et p le nombre de personnages de la liste de voisins
 * 
 * @param world 
 * @param choose
 * @param character 
 */

void Suicide(World *world, VertexArmy *character);

/**
 * @brief Cette fonction immobilise un personnage
 * O(1)
 * 
 * @param world 
 * @param character 
 */

void Immobility(World *world, VertexArmy *character);

/**
 * @brief Cette fonction décrémente les compteurs des chateaux pour la production de leurs éléments
 * O(n) avec n le nombre de chateaux de la couleur
 * 
 * @param list 
 * @param world 
 */

void DecreeAllCastles(ListDChainedCastle* list, World* world);

/**
 * @brief Cette fonction implémente la production de trésor par les manants immobiles en début de tour
 * O(n) avec n le nombre de personnages de la couleur
 * 
 * @param list 
 * @param world 
 */

void PeasantsProdTreasure(ListDChainedCastle *list, World *world);

/**
 * @brief Cette fonction remet les taches des personnages à '0' à la fin d'un tour de jeu, pour qu'ils puissent à nouveau jouer
 * au tour suivant.
 * O(n) avec n le nombre de personnages de la couleur
 * 
 * @param world 
 */

void TasksToZero(World* world);

/**
 * @brief Cette fonction réalise tous les déplacements des personnages n'ayant pas atteint leur destination, en début de tour,
 * O(n) avec n le nombre d'unités en déplacement de la couleur associée, et plus si les déplacements donnent lieu à des combats
 * 
 * 
 * @param list 
 * @param world 
 */

void MovingTurnBeginning(ListDChainedCastle *list, World *world);

/**
 * @brief Cette fonction permet de réaliser le combat entre 2 utités, quel que soit leur type.
 * Complexité dépendant du type de combat, voir les autres fonction de combat
 * 
 * 
 * @param world 
 * @param coord 
 * @return int 
 * 1 s'il n'y a pas eu de combat
 * 0 s'il y a eu un combat
 * -2 si le combat achève la partie
 */

int Fight(World *world, Coordinates coord,char choose);

/**
 * @brief Cette fonction met en oeuvre le combat entre un personnage et un chateau,
 * O(n) avec n le nombre total d'unités de la partie
 * 
 * @param world 
 * @param character 
 * @param castle 
 * @param choose 
 * @return Castle* , la fonction retourne le chateau s'il est vainqueur, NULL sinon
 */

Castle *FightCastle(World *world, VertexArmy *character, Castle *castle, char choose);

/**
 * @brief Cette fonction supprime un chateau qui a été détruit au combat
 * O(n+p) avec n la taille de la liste du chateau détruit, p le nombre de personnages de la couleur du personnage détruisant le chateau
 * 
 * @param world 
 * @param castle 
 * @param choose
 * @param character 
 */

void DeleteCastle(World *world, Castle *castle, VertexArmy *character);

/**
 * @brief Cette fonction implémente toutes les choses devant être faites à la fin d'un tour
 * O(n) avec n le nombre total d'unités de la partie
 * 
 * @param world 
 */

void ChangeTurn(World* world);

/**
 * @brief Cette fonction permet la création des éléments produits par les chateaux à la fin du temps de production,
 * O(n) avec n la taille de la liste
 * 
 * @param list 
 * @param world 
 */

void EndProdOfCastles(ListDChainedCastle* list, World* world);

/**
 * @brief Cette fonction fait combattre, si besoin, les manants qui ont changé d'allégence
 * O(n+p) avec n le nombre de manants ayant changé d'allégence et p le nombre de personnages sur les listes de voisins où sont les manants
 * 
 * @param list , la liste des manants ayant changé d'allégence
 * @param world 
 */

void PeasantFight(ListDChainedArmy *list, World *world);

/**
 * @brief Cette fonction change d'allégence tous les manants associés à un chateau qui a été détruit
 * O(n+p) avec n la taille de la liste du chateau détruit, p le nombre de personnages de la couleur du personnage détruisant le chateau
 * 
 * @param character 
 * @param castle 
 * @param world 
 * @return ListDChainedArmy* , la liste renvoyée est celle de tous les manants qui ont changé d'allégence
 */

ListDChainedArmy *AllegoryChangePeasants(VertexArmy *character, Castle *castle, World *world);


/**
 * @brief Cette fonction ajoute une tête à une liste de personnages, mais en prenant un maillon et pas un pointeur sur un maillon,
 * O(1)
 * 
 * @param list 
 * @param vertex 
 */

void AddHeadArmyWithVertexWithoutPointer(ListDChainedArmy *list, VertexArmy vertex);

/**
 * @brief Cette fonction retrouve l'indice d'un chateau dans la liste des chateaux de sa couleur.
 * O(n) avec n la taille de la liste
 * 
 * @param world 
 * @param c 
 * @return int , l'indice du chateau
 */

int FindIndexCastle(World *world, Castle *c);

/**
 * @brief Cette fonction trie une liste de personnages, en mettant en queue les personnages n'ayant pas encore joué
 * Elle est appelée avec une liste déjà triée selon le type (voir plus tard),
 * O(n) avec n la taille de la liste
 * 
 * @param list 
 */

void SortListCharOcc(ListDChainedArmy *list);

/**
 * @brief Cette fonction trie une liste de personnages selon le type, c'est-à-dire que les seigneurs sont mis avant les guerriers,
 * eux-mêmes avant les manants, puis la fonction trie la liste selon ceux qui ont déjà joué ou non (fonction précédente),
 * O(n) avec n la taille de la liste
 * 
 * @param list 
 */

void SortListCharType(ListDChainedArmy *list);

#endif