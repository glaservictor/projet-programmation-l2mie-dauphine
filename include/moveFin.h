#include<stdlib.h>
#include"World.h"
#include"dijkstra.h"
#include"disGraph.h"

#ifndef MOVE_H
#define MOVE_H

/**
 * @brief Génère une fonction père entre deux sommets du plateau de jeu esquivant les ennemis si c'est possible Ω(n²) -- dim
 * 
 * @param world 
 * @param dep_x 
 * @param dep_y 
 * @param arr_x 
 * @param arr_y 
 * @param c 
 * @return int* 
 */
int* computeMove(World *world,int dep_x,int dep_y,int arr_x,int arr_y, char c);
#endif