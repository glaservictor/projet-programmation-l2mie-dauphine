#include"ListChainedHeritageCastle.h"
#include"ListChainedHeritageCastle.h"
#include"Coord.h"
#include"World.h"

#ifndef INIT_H
#define INIT_H

/**
 * @brief Initialise un chateau, O(1)
 * 
 * @param n 
 * @param c 
 * @param coord 
 * @return Castle* 
 */

Castle* InitCastle(int n, char c, Coordinates coord);

/**
 * @brief Initialise une case, O(1)
 * 
 * @param coord 
 * @param castle 
 * @param list 
 * @return Case* 
 */

Case *InitCase(Coordinates coord, Castle *castle,ListDChainedArmy *list);

/**
 * @brief Initialise une couleur, O(1)
 * 
 * @param c 
 * @return Color* 
 */

Color* InitColor(char c);


/**
 * @brief Initialise le plateau de jeu, O(i*j) avec i le nombre de lignes et j le nombre de colonnes
 * 
 * @param i 
 * @param j 
 * @return Board* 
 */

Board *InitBoard(int i, int j);

/**
 * @brief Initialise le monde de jeu, O(i*j) avec i le nombre de lignes et j le nombre de colonnes
 * 
 * @param i 
 * @param j 
 * @return World* 
 */

World* InitWorld(int i, int j);

#endif