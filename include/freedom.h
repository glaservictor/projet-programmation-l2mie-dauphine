#include <stdio.h>
#include <stdlib.h>
#include "World.h"
#include "castleWorld.h"
#include "ListChainedHeritageCastle.h"
#include "Coord.h"


#ifndef FREEDOM_H
#define FREEDOM_H

#ifndef PAIRC_H
#include"windowsManage.h"
#endif 

/**
 * @brief Libère une coordonnée, O(1)
 * 
 * @param coord 
 */

void FreeCoord(Coordinates *coord);

/**
 * @brief Libère un personnage, O(1)
 * 
 * @param character 
 */

void FreeCharacter(Characters *character);

/**
 * @brief Libère un chateau, O(n) avec n le nombre de personnages de sa liste d'éléments
 * 
 * @param castle 
 */

void FreeCastle(Castle *castle);

/**
 * @brief Libère un maillon de personnage, O(1)
 * 
 * @param vertex 
 */

void FreeVertexArmy(VertexArmy *vertex);

/**
 * @brief Libère un maillon de chateau, O(n) avec n le nombre de personnages de sa liste d'éléments
 * 
 * @param vertex 
 */

void FreeVertexCastle(VertexCastle *vertex);

/**
 * @brief Libère une liste de personnages, O(n) avec n la taille de la liste
 * 
 * @param list 
 */

void FreeListArmy(ListDChainedArmy *list);

/**
 * @brief Libère une liste de chateaux, O(n) avec n le nombre total d'unités associées à la liste 
 * 
 * @param list 
 */

void FreeCastleList(ListDChainedCastle *list);

/**
 * @brief Libère un Pairc, O(1)
 * 
 * @param p 
 */

void FreePairc(struct Pairc *p);

/**
 * @brief Libère une structure de couleur, O(n) avec n le de d'unités associées à la couleur
 * 
 * @param color 
 */

void FreeColor(Color *color);

/**
 * @brief Libère une structure de case, O(n) avec n le nombres de personnages de la liste de voisins
 * 
 * @param c 
 */

void FreeCase(Case *c);

/**
 * @brief Libère le plateau de jeu, O(i*j + n) avec i le nombre de lignes, j le nombre de colonnes, et n le nombre d'unités
 * 
 * @param board 
 */

void FreeBoard(Board *board);

/**
 * @brief Libère le monde du jeu, O(i*j + n) avec i le nombre de lignes, j le nombre de colonnes, et n le nombre d'unités 
 * 
 * @param world 
 */

void FreeWorld(World *world);

#endif