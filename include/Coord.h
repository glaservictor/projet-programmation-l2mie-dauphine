#include<stdio.h>
#include<stdlib.h>


#ifndef COORD_H
#define COORD_H

/**
 * @brief Strucutre représentant les coordonnées du plateau de jeu
 * 
 */
typedef struct Coordinates {

    int x;
    int y;
}Coordinates;

#endif 
