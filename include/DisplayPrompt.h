#include"World.h"


#ifndef PROMPT_H
#define PROMPT_H


/**
 * @brief Recupère les coordonnées du terminal
 * 
 * @return Coordinates* 
 */
Coordinates* GetDimTerminal();


/**
 * @brief Initialise le tableau portant les objets du monde en lui allouant l'espace necesaire
 * 
 * @param world 
 * @return char** 
 */
char** InitDisplay(World* world);


/**
 * @brief Reccupère les chateaux présent dans le plateau et les stock sur une chaine de caractère O(n²) -- n = dim
 * 
 * @param world 
 * @param type 
 * @param display 
 */
void DisplayCastle(World *world,char type, char **display);


/**
 * @brief Recupère les unités présentent sur un plateau et les stock sur un pointeur de pointeur  O(n) -- #castle
 * 
 * @param world 
 * @param color 
 * @return char** 
 */
char** DisplayArmy(World *world, char color);



/**
 * @brief Esquisse d'un affichage console ( sans instruction ) O(n²m) -- m = #army, n = dim
 * 
 * @param world 
 */
void DisplayPrompt(World *world);


/**
 * @brief Esquisse d'un affichage des unités présentes sur le plateau ( experimental ) O(n²m)
 * 
 * @param world 
 * @param color 
 * @return int** 
 */
int** DisplayLegend(World* world,char color);

#endif