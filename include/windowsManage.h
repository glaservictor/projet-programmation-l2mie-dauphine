#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "SDL2/SDL_image.h"
#include "score.h"
#include "DisplayPrompt.h"
#include "World.h"
#include "saveFile.h"
#include "time.h"
#define grid_cell_size 100
#define grid_width 10
#define grid_height 10
#define T_MAX 255
#include "Exec.h"
#include "Coord.h"
#include "World.h"

#ifndef GUI_H
#define GUI_H


#ifndef PAIRC_H
#define PAIRC_H

struct Pairc
{
    struct Coordinates *init;
    struct Coordinates *cur;
};
#include"freedom.h"

#endif 


/**
 * @brief Gestion de la pause ( sauvegardes, reprise...)
 * 
 * @param world 
 * @param window 
 * @param renderer 
 * @param police 
 * @return int 
 */
int PauseMenue(World *world, SDL_Window *window, SDL_Renderer *renderer, TTF_Font *police);


/**
 * @brief Gestion principal du plateau de jeu ( unités , chateaux )
 * 
 * @param world 
 * @param renderer 
 * @param window 
 * @return int 
 */
int windowsManage(World *world, SDL_Renderer *renderer, SDL_Window *window);

/**
 * @brief Affichage du menu principal 
 * 
 * @param world 
 */
void DisplayMenue(World *world);

#endif