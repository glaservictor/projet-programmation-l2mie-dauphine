#include <stdio.h>
#include "World.h"
#include "Coord.h"

#define SIZE 10

#ifndef PATH_H
#define PATH_H

typedef struct Pair
{
    int x;
    int y;
} Pair;


/**
 * @brief Permet le transfert de donnés deux tableaux de Pair O(n²)
 * @param la source
 * @param le receveur
 * @param la taille du tanleau 
 * 
 */
void equalPairC(Pair *, Pair *, int); 

/**
 * @brief Transforme un plateau de jeu en graph en prenant en compte l'esquive des unités adverse O(n²)
 * 
 * @param board 
 * @param c 
 * @return Pair* 
 */

Pair *gridToGraph(Board *board, char c, Coordinates); 

#endif
