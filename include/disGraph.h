#include<stdio.h>
#include"SDL2/SDL.h"
#include"chosePath.h"
#include <time.h>


#ifndef DISGRAPH_H
#define DISGRAPH_H

#define WIN 600
#define BORDER 20

/**
 * @brief Affichage avec SDL2 d'un graph Ω(n) -- size
 * @param un graphe 
 * @param la taille du graphe associé 
 * @return int 
 */
int DisplayGraph(Pair*, int);

/**
 * @brief Initialise le chemin tracé ( le graphe plus généralement ) entre deux sommet beg et end. Convention pour les coordonnées 
 * est un sommet correspond au porduit de la taille du plateau et de l'abscisse, auxquel on ajoute l'ordonnées
 * e.g: (2,3) pour un plateau 8x8 => 8x2+3 = 19  O(n²)
 * 
 * @param gr 
 * @param beg 
 * @param end 
 * @param size 
 * @return Pair* 
 */
Pair* InitGrapheDjik(int* gr,int beg,int end,size_t size);

#endif 