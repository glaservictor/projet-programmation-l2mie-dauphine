#include<stdlib.h>
#include<stdio.h>

#include"Characters.h"


#ifndef VERTEX_ARMY_H
#define VERTEX_ARMY_H


typedef struct VertexArmy
{
    struct VertexArmy* next;
    struct VertexArmy* prev;
    Characters *army;
}VertexArmy;


typedef struct ListDChainedArmy
{
    VertexArmy* head;
    VertexArmy* tail;
    int size;
}ListDChainedArmy;

#endif 

#ifndef CASTLE_H
#include"castleWorld.h"

#ifndef COLOR_H
#include "World.h"
#endif 


/**
 * @brief Vestige d'une surcharge d'opérateur avant d'en abandonner l'utilité 
 * 
 * @param res 
 * @param pos 
 */


/**
 * @brief Vestige d'une surcharge d'opérateur 
 * 
 * @param res 
 * @param pos 
 */
void equalArmee(VertexArmy *res,VertexArmy *pos);


/**
 * @brief Initialisation d'une liste doublement chainée d'unités ( allocation de l'espace mémoire )
 * 
 * @return ListDChainedArmy* 
 */
ListDChainedArmy* ListInitializationArmy();


/**
 * @brief Initialisation d'un maillon armée d'une liste doublement chainée 
 * 
 * @param p 
 * @param castle 
 * @return VertexArmy* 
 */
VertexArmy* VertexInitializationArmy(Characters *p);

/**
 * @brief Ajoute en queue d'une liste armée doublement chainée un maillon armée
 * 
 * @param list 
 * @param vertex 
 */
void AddTailArmy(ListDChainedArmy* list, VertexArmy* vertex);


/**
 * @brief Ajoute en tête d'une liste armée doublement chaînée un maillon armée 
 * 
 * @param list 
 * @param vertex 
 */
void AddHeadArmy(ListDChainedArmy *list, VertexArmy *vertex);


/**
 * @brief Supprime de la liste armée un maillon d'indice n
 * 
 * @param list 
 * @param n 
 * @return VertexArmy* 
 */
VertexArmy* DeleteListArmy(ListDChainedArmy* list, int n);


/**
 * @brief Ajoute à la liste doublement chainée un maillon en indice n
 * 
 * @param list 
 * @param vertex 
 * @param n 
 * @return ListDChainedArmy* 
 */
ListDChainedArmy* AddListArmy(ListDChainedArmy* list, VertexArmy* vertex, int n);


/**
 * @brief Affiche une donnée associé au maillon de la liste doublement chainée
 * 
 * @param list 
 */
void DisplayListArmy(ListDChainedArmy *list);


/**
 * @brief Créer un guerrier et l'ajoute à ses structures associées 
 * 
 * @param castle 
 * @param Color 
 * @param x 
 * @param y 
 * @return VertexArmy* 
 */
VertexArmy* CreateWarrior(Castle* castle, char Color,int x,int y);


/**
 * @brief Créer un Seigneur et l'ajoute à ses structures associées
 * 
 * @param castle 
 * @param Color 
 * @return VertexArmy* 
 */
VertexArmy* CreateLord(Castle* castle, char Color,int ,int );


/**
 * @brief Créer un manant et l'ajoute à ses structures associées
 * 
 * @param castle 
 * @param Color 
 * @return VertexArmy* 
 */
VertexArmy* CreatePeasant(Castle* castle, char Color,int ,int);


/**
 * @brief Trie une liste chainé armée par type de caractère 
 * ( SEIGNEUR < GUERRIER < MANANT )
 * 
 * @param list 
 */
void SortListCharType(ListDChainedArmy* list);


/**
 * @brief Tri la liste en fonction de la disponibilité des caractères
 * ( OCCUPE < LIBRE )
 * @param list 
 */
void SortListCharOcc(ListDChainedArmy* list);

#endif 
