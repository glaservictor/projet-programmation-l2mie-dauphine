#include<stdio.h>
#include<stdlib.h>
#include"World.h"

#ifndef SCORE_H
#define SCORE_H


/**
 * @brief Calcul du score selon le barème suivant :
 * SCORE = UNITE x COUP DE PRODUCTION + TRESOR + NOMBRE CHATEAU x COUP DE PRODUCTION
 * O(nm) -- #army & #castle
 * @param world 
 * @param c 
 * @return int 
 */
int calculateScore(World *world, char c);



/**
 * @brief Fonction d'écriture du score O(n) #char
 * 
 * @param world 
 * @return int 
 */
    int scoreManage(World *world);

    /**
 * @brief Fonction principale associée à la gestion du score O(n) #char
 * 
 * @param file 
 * @param world 
 * @return int 
 */
    int saveScore(FILE *file, World *world);

#endif