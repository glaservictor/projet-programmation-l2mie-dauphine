#include "World.h"
#include "ListChainedHeritageCastle.h"
#include "ListChainedHeritageCastle.h"
#include "Exec.h"

#ifndef SAVE_H
#define SAVE_H

/**
 * @brief Ecrit en mémoire les information relative à un personnage du plateau  Ω(n) -- #army 
 * 
 * @param file 
 * @param vertex 
 * @param c 
 */
void displayElement(FILE *file, VertexCastle *vertex, char c);

/**
 * @brief Fichier principal en charge des sauvegardes  Ω(n*m) -- #castle & #army
 * 
 * @param world 
 * @return int 
 */
int fileManage(World *world);

/**
 * @brief Fichier principal en charge des chargement de sauvegardes  Ω(n*m) -- #castle & #army
 * 
 * @return World* 
 */
World *loadGame();

/**
 * @brief Fonction d'écriture de la sauvegardes lignes par lignes O(n) -- #char
 * 
 * @param file 
 * @param world 
 * @return int 
 */
int writeSave(FILE *file, World *world);


/**
 * @brief Prends les coordonnées et destination en fonction d'un fichier O(n) -- #char
 * 
 * @param world 
 * @param file 
 * @return int* 
 */
int *getCoord(World *world, FILE *file);

#endif