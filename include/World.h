#include<stdio.h>
#include<stdlib.h>
#include"listChained.h"
#include"ListChainedHeritageArmy.h"
#include"ListChainedHeritageCastle.h"


#ifndef WORLD_F_H
#define WORLD_F_H

#ifndef BOARD_H
#define BOARD_H

#include"ListChainedHeritageCastle.h"
#ifdef LIST_CASTLE_H
#ifndef COLOR_H
#define COLOR_H


void equalCastle(Castle*,Castle*);

/**
 * @brief Structure représentant tout ce qui est associé à une couleur (équipe)
 * 
 */

typedef struct Color {
    char color;
    ListDChainedCastle *castle;
    int treasure;
    char turn; /* si c'est à son turn de jouer ou non */
} Color ;

#endif 

/**
 * @brief Structure modélisant une case du plateau de jeu et ce qui lui est associé
 * 
 */

typedef struct Case 
{
    Coordinates coord;
    Castle* castle;
    ListDChainedArmy *neighbours;
}Case;

/**
 * @brief Structure modélisant le plateau de jeu
 * 
 */

typedef struct Board 
{
    int lines;
    int rows; /* Si jamais on veut changer la taille du board */
    struct Case **matrix;
} Board;

/**
 * @brief Structure miodélisant le monde du jeu
 * 
 */

typedef struct World {

    Board *board;
    Color *blue;
    Color *red;
    char turn;
} World;



#endif 


/**
 * @brief Fonction renvoyant en entier le nombre correspondant à l'enum Type
 * O(1)
 * 
 * @param type 
 * @return int 
 */

int TypeToInt(Type type);

/**
 * @brief Fonction initialisant un chateau
 * O(1)
 * 
 * @param n 
 * @param c 
 * @param coord 
 * @return struct Castle* 
 */

struct Castle* InitCastle(int n, char c, Coordinates coord);

/**
 * @brief Fonction initialisant une case
 * O(1)
 * 
 * @param coord 
 * @param castle 
 * @param list 
 * @return Case* 
 */

Case *InitCase(Coordinates coord, struct Castle *castle,ListDChainedArmy *list);

/**
 * @brief Fonction initialisant une couleur
 * O(1)
 * 
 * @param c 
 * @return Color* 
 */

Color* InitColor(char c);

/**
 * @brief Initialise le plateau de jeu selon ses dimensions
 * O(1)
 * 
 * @param i 
 * @param j 
 * @return Board* 
 */

Board *InitBoard(int i, int j);

/**
 * @brief Initialise le monde selon les dimensions du plateau
 * O(1)
 * 
 * @param i 
 * @param j 
 * @return World* 
 */

World* InitWorld(int i, int j);

/**
 * @brief Change la destination d'un personnage
 * O(1)
 * 
 * @param character 
 * @param dest 
 */

void NewDestChar(VertexArmy* character, Coordinates dest);

/**
 * @brief Permet la création d'un élément par un chateau à la fin du temps de production
 * O(1)
 * 
 * @param castle 
 * @param world 
 */

void CastleBear(Castle* castle, World* world);

/**
 * @brief Commence la production d'un guerrier
 * O(1)
 * 
 * @param castle 
 * @param world 
 */


void ProdWarrior(Castle* castle, World* world);

/**
 * @brief Commence la production d'un manant
 * O(1)
 * 
 * @param castle 
 * @param world 
 */

void ProdPeasant(Castle* castle, World* world);

/**
 * @brief Commence la production d'un seigneur
 * O(1)
 * 
 * @param castle 
 * @param world 
 */

void ProdLord(Castle* castle, World* world);


#endif
#endif 

