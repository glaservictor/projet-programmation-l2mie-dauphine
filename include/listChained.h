#include <stdio.h>
#include <stdlib.h>

#ifndef LISTE_H
#define LISTE_H

typedef struct ListDChained
{
    struct Vertex* head;
    struct Vertex* tail;
    int size;
}ListDChained;

typedef struct Vertex
{
    struct Vertex* next;
    struct Vertex* prev;
    int val;
}Vertex;


/**
 * @brief Initialise en allouant l'espace mémoire necessaire une liste doublement chainée O(1)
 * 
 * @return ListDChained* 
 */
ListDChained* ListInitialization();


/**
 * @brief Initialise en allouant l'espace mémoire necessaire un maillon d'une liste doublement chainée O(1)
 * 
 * @return Vertex* 
 */
Vertex* VertexInitialization();


/**
 * @brief Ajoute en queue de liste un maillon O(1)
 * 
 * @param list 
 * @param vertex 
 */
void AddTail(ListDChained* list, Vertex* vertex);


/**
 * @brief Ajoute en tête de liste un maillon O(1)
 * 
 * @param list 
 * @param vertex 
 */
void AddHead(ListDChained *list, Vertex *vertex);


/**
 * @brief Supprime de la liste un maillon d'indice n O(n)
 * 
 * @param list 
 * @param n 
 * @return Vertex* 
 */
Vertex* DeleteList(ListDChained* list, int n);


/**
 * @brief Ajoute à la liste un maillon en position n O(n)
 * 
 * @param list 
 * @param vertex 
 * @param n 
 * @return ListDChained* 
 */
ListDChained* AddList(ListDChained* list, Vertex* vertex, int n);


/**
 * @brief Affiche les valeurs associés aux maillons de la liste O(n)
 * 
 * @param list 
 */
void DisplayList(ListDChained *list);

#endif 
