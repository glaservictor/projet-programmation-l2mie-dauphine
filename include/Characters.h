#include<stdio.h>
#include<stdlib.h>
#include"Coord.h"

#ifndef PERSONNAGE_H
#define PERSONNAGE_H


/**
 * @brief Permet de caractériser le type d'un personnage
 * 
 */

typedef enum Type {
    Lord = 0,
    Warrior = 1,
    Peasant = 2,

}Type;

/**
 * @brief Cette structure sert à représenter tous les personnages
 * 
 */


typedef struct Characters {

    enum Type type; 
    char color; /* de l'équipe */
    char task; /* '0' s'il peut jouer, '1' sinon */
    char alive; 
    Coordinates destination;
    Coordinates coord;
    char order; /* détaille l'action réalisée */
    int attack;

} Characters;


#endif


void equal(Characters *res,Characters *pos );




