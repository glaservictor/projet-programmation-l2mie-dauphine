#include<stdio.h>

#ifndef VERTEX_ARMY_H
#include"ListChainedHeritageArmy.h"
#endif 

#ifdef VERTEX_ARMY_H
#include "listChained.h"

#ifndef CASTLE_H
#define CASTLE_H

/**
 * @brief Cette structure sert à représenter les chateaux
 * 
 */

typedef struct Castle 
{
    int number; /* indice du castle dans la list */
    char color; /* de l'équipe */
    int counter; /* le nombre de tours restant avant la fin de production d'un élément */
    char task;  /* type en production ou 0 */
    char alive; /* ou mort */
    ListDChainedArmy *elements; /* les personnages associés */
    Coordinates coord;
    int attack;

}Castle;

/**
 * @brief Fonction changeant la destination d'un personnage, O(1)
 * 
 * @param character 
 * @param dest 
 */

void NewDestChar(VertexArmy* character, Coordinates dest);

/**
 * @brief Fonction décrémentant le compteur de production d'un chateau, O(1)
 * 
 * @param castle 
 */

void DecreeCastle(Castle* castle);

/**
 * @brief Fonction faisant la conversion entre un Type et un int, selon les valeurs données dans le Type, O(1)
 * 
 * @param type 
 * @return int 
 */

int TypeToInt(Type type);

#endif 
#endif