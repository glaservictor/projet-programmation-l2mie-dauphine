#include "saveFile.h"

void displayElement(FILE *file, VertexCastle *vertex, char c)
{
    VertexArmy *maillonA = vertex->castle->elements->head;
    while (maillonA != NULL)
    {
        fprintf(file, "%c ", c + 'A' - 'a');
        switch (maillonA->army->type)
        {
        case Lord:
            fprintf(file, "s");
            break;
        case Warrior:
            fprintf(file, "g");
            break;
        case Peasant:
            fprintf(file, "m");
            break;
        }
        fprintf(file, " %d %d %d %d\n", maillonA->army->coord.x, maillonA->army->coord.y, maillonA->army->destination.x, maillonA->army->destination.y);
        maillonA = maillonA->next;
    }
}

int writeSave(FILE *file, World *world)
{
    fprintf(file, "%d %d\n", world->board->lines, world->board->rows);
    VertexCastle *vertex = NULL;
    if (world->turn == 'b')
    {
        fprintf(file, "B %d \n", world->blue->treasure);
        fprintf(file, "R %d \n", world->red->treasure);
    }
    else
    {
        fprintf(file, "R %d \n", world->red->treasure);
        fprintf(file, "B %d \n", world->blue->treasure);
    }
    vertex = world->blue->castle->head;
    while (vertex != NULL)
    {
        fprintf(file, "B c %d %d %c %d \n", vertex->castle->coord.x, vertex->castle->coord.y, vertex->castle->task, vertex->castle->counter);
        displayElement(file, vertex, 'b');
        vertex = vertex->next;
    }
    vertex = world->red->castle->head;
    while (vertex != NULL)
    {
        fprintf(file, "R c %d %d %c %d \n", vertex->castle->coord.x, vertex->castle->coord.y, vertex->castle->task, vertex->castle->counter);
        displayElement(file, vertex, 'r');
        vertex = vertex->next;
    }
    return 1;
}

int fileManage(World *world)
{
    FILE *file = fopen("save.got", "w");
    if (file != NULL)
    {
        int s = writeSave(file, world);
        if (s != 1)
            return -1;
        fclose(file);
        return 1;
    }
    return 0;
}

int *getCoord(World *world, FILE *file)
{
    char c;
    int index = 0, *tab = malloc(sizeof(int) * 4);
    while ((c = fgetc(file)) != '\n' && c != EOF)
    {
        if (c != ' ')
        {
            if (c - '0' >= 0 && c - '9' <= 0)
            {
                tab[index] = atoi(&c);
                index++;
            }
            if (c == '-')
            {
                tab[index] = -1;
                tab[++index] = -1;
                return tab;
            }
            switch (c)
            {
            case 'l':
                tab[index] = 'l';
                ++index;
                break;
            case 'm':
                tab[index] = 'm';
                ++index;
                break;
            case 's':
                tab[index] = 's';
                ++index;
                break;
            }
        }
    }
    return tab;
}

World *loadGame()
{
    int i = 0, a, majToMin = 'A' - 'a';
    FILE *file = fopen("save.got", "r");
    if (file == NULL)
        return NULL;
    char *size = malloc(2 * sizeof(char)), *leftover = malloc(10 * sizeof(char));
    char c, color;
    while ((c = fgetc(file)) != '\n')
    {
        size[i] = c;
        i++;
    }
    i = 0;
    int n = strtol(size, &leftover, 10);
    World *world = InitWorld(n, n);
    char *arr = calloc(255, sizeof(char));
    for (int j = 0; j < 2; j++)
    {
        color = fgetc(file);
        if (j == 0)
            world->turn = color - majToMin;
        while ((c = fgetc(file)) != '\n')
        {
            arr[i] = c;
            i += 1;
        }
        a = atoi(arr);
        if (color == 'R')
            world->red->treasure = a;
        else
            world->blue->treasure = a;
        i = 0;
    }
    int *tab = malloc(4 * sizeof(int));
    Castle *castle = malloc(sizeof(castle));
    Coordinates coord, co1;
    int com;
    char task;
    VertexCastle *VertexCastle = malloc(sizeof(VertexCastle));
    while ((c = fgetc(file)) != EOF)
    {
        switch (c)
        {
        case 'B':
            color = 'b';
            break;
        case 'R':
            color = 'r';
            break;
        case 'c':
            tab = getCoord(world, file);
            coord.x = tab[0];
            coord.y = tab[1];
            castle = InitCastle(1, color, coord);
            castle->task = tab[2];
            castle->counter = tab[3];
            VertexCastle = VertexInitCastle(castle);
            if (color == 'r')
                AddHeadCastle(world->red->castle, VertexCastle);
            else
                AddHeadCastle(world->blue->castle, VertexCastle);
            world->board->matrix[tab[0]][tab[1]].castle = castle;
            break;
        case 'g':
            com = castle->counter;
            task = castle->task;
            tab = getCoord(world, file);
            coord.x = tab[0];
            coord.y = tab[1];
            co1.x = tab[2];
            co1.y = tab[3];
            if (color == 'r')
                AddChar('r', world, Warrior, coord, co1, castle);
            else
                AddChar('b', world, Warrior, coord, co1, castle);
            castle->task = task;
            castle->counter = com;
            break;
        case 's':
            com = castle->counter;
            task = castle->task;
            tab = getCoord(world, file);
            coord.y = tab[1];
            coord.x = tab[0];
            co1.x = tab[2];
            co1.y = tab[3];
            if (color == 'r')
                AddChar('r', world, Lord, coord, co1, castle);
            else
                AddChar('b', world, Lord, coord, co1, castle);
            castle->task = task;
            castle->counter = com;
            break;
        case 'm':
            com = castle->counter;
            task = castle->task;
            tab = getCoord(world, file);
            coord.x = tab[0];
            coord.y = tab[1];
            co1.x = tab[2];
            co1.y = tab[3];
            if (color == 'r')
                AddChar('r', world, Peasant, coord, co1, castle);
            else
                AddChar('b', world, Peasant, coord, co1, castle);
            castle->task = task;
            castle->counter = com;

            break;
        }
    }
    return world;
}
