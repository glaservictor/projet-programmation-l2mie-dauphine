#include "score.h"

int calculateScore(World *world, char c)
{
    int res = 0;
    VertexCastle *tmp = NULL;
    if (c == 'r')
    {
        tmp = world->red->castle->head;
        res = world->red->treasure;
    }
    else
    {
        tmp = world->blue->castle->head;
        res = world->blue->treasure;
    }
    for (; tmp != NULL; tmp = tmp->next)
    {
        res += 30;
        for (VertexArmy *ele = tmp->castle->elements->head; ele != NULL; ele = ele->next)
        {
            Type type = ele->army->type;
            switch (type)
            {
            case Peasant:
                res += 1;
                break;
            case Warrior:
                res += 5;
                break;
            case Lord:
                res += 20;
                break;
            default:
                break;
            }
        }
    }
    return res;
}

int saveScore(FILE *file, World *world)
{
    FILE *tmp = fopen("tmp", "w+");
    if (tmp == NULL)
        return EXIT_FAILURE;

    char c;
    if ((c = fgetc(file)) == EOF)
        for (int i = 0; i < 10; i++)
            fprintf(file, "0\n");
    int res = 0, score = 0, write = 1;
    if (world->blue->castle == NULL)
        res = calculateScore(world, 'r');
    else if (world->red->castle->head == NULL)
        res = calculateScore(world, 'b');
    char *str = calloc(255, sizeof(char));
    int index = 0;
    fseek(file, SEEK_SET, 0);
    for (int i = 0; i < 10; i++)
    {
        c = ' ';
        while (c != '\n' && c != EOF)
        {
            c = fgetc(file);
            str[index] = c;
            index++;
        }
        {
            score = atoi(str);
            if (score < res && write)
            {
                fprintf(tmp, "%d\n", res);
                if (i != 9)
                    fprintf(tmp, "%d\n", score);
                write = 0;
                i++;
            }
            else
                fprintf(tmp, "%d\n", score);
        }
        index = 0;
    }
    fclose(tmp);
    return 1;
}

int scoreManage(World *world)
{
    FILE *file = fopen("score.txt", "r");
    if (file != NULL)
    {
        int s = saveScore(file, world);
        remove("score.txt");
        rename("tmp", "score.txt");
        if (s != 1)
            return -1;
        fclose(file);
        return 1;
    }
    return 0;
}
