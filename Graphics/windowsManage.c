#include "windowsManage.h"

SDL_Color colorBlanche = {22, 22, 22, 255};
SDL_Color colorRouge = {200, 50, 50, 0};
SDL_Color colorBLeue = {50, 50, 200, 0};

int WINSIZE = 1000;

int buttonManage(World *world, SDL_Window *window, SDL_Renderer *renderer, TTF_Font *police, int x, int y)
{
    SortListCharType(world->board->matrix[x][y].neighbours);
    SDL_Surface *swordS,
        *castleS, *coinS, *crownS;
    castleS = IMG_Load("Graphics/icons/castle.png");
    swordS = IMG_Load("Graphics/icons/sword.png");
    coinS = IMG_Load("Graphics/icons/coin.png");
    crownS = IMG_Load("Graphics/icons/crown.png");
    int character = 0;
    SDL_Rect win = {WINSIZE / 4, WINSIZE / 12, WINSIZE / 2, WINSIZE * 0.8};
    //SDL_Rect rest = {0, 0, 2 * win.w, win.h};
    int maxy, maxx, miny, minx, advance;
    if (TTF_GlyphMetrics(police, 'Q', &minx, &maxx, &miny, &maxy, &advance) == -1)
        printf("%s\n", TTF_GetError());

    SDL_Rect square1 = {win.x + 0.25 * win.w, win.y + 0.2 * WINSIZE, win.w / 2, win.h / 10};
    SDL_Rect square2 = {win.x + 0.25 * win.w, win.y + 0.3 * WINSIZE, win.w / 2, win.h / 10};
    SDL_Rect square3 = {win.x + 0.25 * win.w, win.y + 0.4 * WINSIZE, win.w / 2, win.h / 10};
    SDL_Rect square4 = {win.x + 0.25 * win.w, win.y + 0.5 * WINSIZE, win.w / 2, win.h / 10};

    int castle = 0;
    Coordinates cood;
    int res;

    if (world->board->matrix[x][y].neighbours != NULL && world->board->matrix[x][y].neighbours->tail != NULL && world->board->matrix[x][y].neighbours->tail != NULL && world->board->matrix[x][y].neighbours->tail->army->task == '0' && world->board->matrix[x][y].neighbours->tail->army->color == world->turn)
        character = 1;
    if (world->board->matrix[x][y].castle != NULL && world->board->matrix[x][y].castle->task == '0' && world->board->matrix[x][y].castle->color == world->turn)
        castle = 1;
    SDL_Color color = {255, 255, 255, 255};
    char *st = malloc(T_MAX * sizeof(char));
    sprintf(st, "DESTINATION");
    SDL_Surface *destination = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tdestination = SDL_CreateTextureFromSurface(renderer, destination);
    sprintf(st, "MANANT");
    SDL_Surface *peasant = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tmanant = SDL_CreateTextureFromSurface(renderer, peasant);

    sprintf(st, "SUICIDE");
    SDL_Surface *suicide = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tsuicide = SDL_CreateTextureFromSurface(renderer, suicide);
    sprintf(st, "SEIGNEUR");
    SDL_Surface *lord = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tlord = SDL_CreateTextureFromSurface(renderer, lord);

    sprintf(st, "IMMOBILITY");
    SDL_Surface *immobility = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *timmobility = SDL_CreateTextureFromSurface(renderer, immobility);
    sprintf(st, "GUERRIER");
    SDL_Surface *warrior = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *twarrior = SDL_CreateTextureFromSurface(renderer, warrior);

    SDL_bool seign = SDL_FALSE;
    SDL_Texture *tdetail = NULL;
    SDL_Surface *detail = NULL;

    if (!castle && !character)
    {
        res = 0;
        goto ret;
    }

    SDL_SetRenderDrawColor(renderer, 44, 44, 44, 255);
    SDL_RenderFillRect(renderer, &win);

    if (character)
    {
        square1.w = destination->w;
        square1.x = win.x + (win.w - destination->w) / 2;
        square1.h = destination->h;
    }
    else if (castle)
    {
        square1.w = peasant->w;
        square1.x = win.x + (win.w - peasant->w) / 2;
        square1.h = peasant->h;
    }
    if (character)
    {
        square2.w = suicide->w;
        square2.x = win.x + (win.w - suicide->w) / 2;
        square2.h = suicide->h;
    }
    else if (castle)
    {
        square2.w = lord->w;
        square2.x = win.x + (win.w - lord->w) / 2;
        square2.h = lord->h;
    }

    if (character)
    {
        square3.w = immobility->w;
        square3.x = win.x + (win.w - immobility->w) / 2;
        square3.h = immobility->h;
    }
    else if (castle)
    {
        square3.w = warrior->w;
        square3.x = win.x + (win.w - warrior->w) / 2;
        square3.h = warrior->h;
    }
    //if character == libre detail = libre sinon occupe en red et en vert
    if (character && world->board->matrix[x][y].neighbours->tail->army->type == Lord)
    {
        seign = SDL_TRUE;
        sprintf(st, "castle");
        detail = TTF_RenderText_Blended(police, st, color);
        tdetail = SDL_CreateTextureFromSurface(renderer, detail);
        square4.w = detail->w;
        square4.x = win.x + (win.w - detail->w) / 2;
        square4.h = detail->h;
    }
    SDL_Color colorJaune = {255, 255, 0, 255};
    if (character)
    {
        if (tdetail != NULL)
            SDL_RenderCopy(renderer, tdetail, NULL, &square4);
        SDL_RenderCopy(renderer, timmobility, NULL, &square3);
        SDL_RenderCopy(renderer, tsuicide, NULL, &square2);
        SDL_RenderCopy(renderer, tdestination, NULL, &square1);
    }
    else if (castle)
    {
        if (tdetail != NULL)
            SDL_RenderCopy(renderer, tdetail, NULL, &square4);
        SDL_RenderCopy(renderer, tlord, NULL, &square3);
        SDL_RenderCopy(renderer, twarrior, NULL, &square2);
        SDL_RenderCopy(renderer, tmanant, NULL, &square1);
    }

    SDL_bool _quit = SDL_FALSE;
    SDL_Event event;
    while (!_quit)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE)
                {
                    res = 0;
                    _quit = SDL_TRUE;
                    goto ret;
                }
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&cood.x, &cood.y);
                if (cood.x > square1.x && cood.x < square1.x + square1.w)
                {
                    if (cood.y > square1.y && cood.y < square1.y + square1.h)
                    {
                        if (castle)
                        {
                            peasant = TTF_RenderText_Blended(police,
                                                             "MANANT", colorJaune);
                            tmanant = SDL_CreateTextureFromSurface(renderer, peasant);
                        }
                        else
                        {
                            destination = TTF_RenderText_Blended(police,
                                                                 "DESTINATION", colorJaune);
                            tdestination = SDL_CreateTextureFromSurface(renderer, destination);
                        }
                    }
                    else
                    {
                        if (castle)
                        {
                            peasant = TTF_RenderText_Blended(police,
                                                             "MANANT", color);
                            tmanant = SDL_CreateTextureFromSurface(renderer, peasant);
                        }
                        else
                        {
                            destination = TTF_RenderText_Blended(police,
                                                                 "DESTINATION", color);
                            tdestination = SDL_CreateTextureFromSurface(renderer, destination);
                        }
                    }
                    if (cood.y > square2.y && cood.y < square2.y + square2.h)
                    {
                        if (castle)
                        {
                            lord = TTF_RenderText_Blended(police,
                                                          "SEIGNEUR", colorJaune);
                            tlord = SDL_CreateTextureFromSurface(renderer, lord);
                        }
                        else
                        {
                            suicide = TTF_RenderText_Blended(police,
                                                             "SUICIDE", colorJaune);
                            tsuicide = SDL_CreateTextureFromSurface(renderer, suicide);
                        }
                    }
                    else
                    {
                        if (castle)
                        {
                            lord = TTF_RenderText_Blended(police,
                                                          "SEIGNEUR", color);
                            tlord = SDL_CreateTextureFromSurface(renderer, lord);
                        }
                        else
                        {
                            suicide = TTF_RenderText_Blended(police,
                                                             "SUICIDE", color);
                            tsuicide = SDL_CreateTextureFromSurface(renderer, suicide);
                        }
                    }
                    if (cood.y > square3.y && cood.y < square3.y + square3.h)
                    {
                        if (castle)
                        {
                            warrior = TTF_RenderText_Blended(police,
                                                             "GUERRIER", colorJaune);
                            twarrior = SDL_CreateTextureFromSurface(renderer, warrior);
                        }
                        else
                        {
                            immobility = TTF_RenderText_Blended(police,
                                                                "IMMOBILITY", colorJaune);
                            timmobility = SDL_CreateTextureFromSurface(renderer, immobility);
                        }
                    }
                    else
                    {
                        if (castle)
                        {
                            warrior = TTF_RenderText_Blended(police,
                                                             "GUERRIER", color);
                            twarrior = SDL_CreateTextureFromSurface(renderer, warrior);
                        }
                        else
                        {
                            immobility = TTF_RenderText_Blended(police,
                                                                "IMMOBILITY", color);
                            timmobility = SDL_CreateTextureFromSurface(renderer, immobility);
                        }
                    }
                    if (seign && cood.y > square4.y && cood.y < square4.y + square4.h)
                    {
                        detail = TTF_RenderText_Blended(police,
                                                        "castle", colorJaune);
                        tdetail = SDL_CreateTextureFromSurface(renderer, detail);
                    }
                    else if (seign)
                    {
                        detail = TTF_RenderText_Blended(police,
                                                        "castle", color);
                        tdetail = SDL_CreateTextureFromSurface(renderer, detail);
                    }
                }
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&cood.x, &cood.y);

                if (cood.x > square1.x && cood.x < square1.x + square1.w && event.button.button == SDL_BUTTON_LEFT)
                {
                    if (cood.y > square1.y && cood.y < square1.y + square1.h)
                    {
                        if (character)
                            res = -1;
                        else
                            res = 1;
                        _quit = SDL_TRUE;
                    }
                    else if (cood.y > square2.y && cood.y < square2.y + square2.h)
                    {
                        if (character)
                            res = -2;
                        else
                            res = 2;
                        _quit = SDL_TRUE;
                    }
                    else if (cood.y > square3.y && cood.y < square3.y + square3.h)
                    {
                        if (character)
                            res = -3;
                        else
                            res = 3;
                        _quit = SDL_TRUE;
                    }
                    else if (cood.y > square4.y && cood.y < square4.y + square4.h && seign)
                    {
                        res = -4;
                        _quit = SDL_TRUE;
                    }
                }
            }
        }
        SDL_RenderClear(renderer);
        SDL_RenderFillRect(renderer, &win);
        if (!castle || character)
        {
            SDL_RenderCopy(renderer, tdetail, NULL, &square4);
            SDL_RenderCopy(renderer, timmobility, NULL, &square3);
            SDL_RenderCopy(renderer, tsuicide, NULL, &square2);
            SDL_RenderCopy(renderer, tdestination, NULL, &square1);
        }
        else
        {
            SDL_RenderCopy(renderer, tdetail, NULL, &square4);
            SDL_RenderCopy(renderer, twarrior, NULL, &square3);
            SDL_RenderCopy(renderer, tlord, NULL, &square2);
            SDL_RenderCopy(renderer, tmanant, NULL, &square1);
        }
        SDL_RenderPresent(renderer);
    }
ret:
    free(st);
    SDL_FreeSurface(detail);
    SDL_FreeSurface(swordS);
    SDL_FreeSurface(castleS);
    SDL_FreeSurface(coinS);
    SDL_FreeSurface(crownS);
    SDL_FreeSurface(warrior);
    SDL_FreeSurface(peasant);
    SDL_FreeSurface(lord);
    SDL_FreeSurface(destination);
    SDL_FreeSurface(suicide);
    SDL_DestroyTexture(twarrior);
    SDL_DestroyTexture(tlord);
    SDL_DestroyTexture(tmanant);
    SDL_DestroyTexture(tdetail);
    SDL_DestroyTexture(tdestination);
    SDL_DestroyTexture(tsuicide);
    SDL_DestroyTexture(timmobility);
    //SDL_FreeSurface(detail); WTF segfault alors que la porté de la surface semble correcte ??
    SDL_FreeSurface(immobility);

    return res;
}

int windowsManage(World *world, SDL_Renderer *renderer, SDL_Window *window)
{
    int colonne = world->board->rows, ligne = world->board->lines;
    TTF_Font *policeP = TTF_OpenFont("Graphics/fonts/nick.ttf", 50);
    char **castleB = malloc(colonne * ligne * sizeof(char));
    char **castleR = malloc(colonne * ligne * sizeof(char));
    char choose = '0';
    for (int i = 0; i < world->board->rows; i++)
    {
        castleB[i] = malloc(colonne * sizeof(char));
        castleR[i] = malloc(colonne * sizeof(char));
    }
    char *st = malloc(T_MAX * sizeof(char));
    TTF_Init();
    IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
    int nbUnite[ligne][colonne][3];
    for (int i = 0; i < ligne * colonne; i++)
        for (int j = 0; j < 3; j++)
            nbUnite[i / ligne][i % ligne][j] = 0;
    SDL_Surface *swordS = NULL, *castleS = NULL, *coinS = NULL, *crownS = NULL;
    castleS = IMG_Load("Graphics/icons/castle.png");
    swordS = IMG_Load("Graphics/icons/sword.png");
    coinS = IMG_Load("Graphics/icons/coin.png");
    crownS = IMG_Load("Graphics/icons/crown.png");
    SDL_bool cursor = SDL_FALSE;
    char win = '0';

    int window_height = (grid_height * grid_cell_size) + 1;
    SDL_Color colorBlanche = {22, 22, 22, 255};
    SDL_Color colorRouge = {200, 50, 50, 0};
    SDL_Color colorBLeue = {50, 50, 200, 0};
    TTF_Font *police = TTF_OpenFont("Graphics/fonts/Rasa-Medium.ttf", 25);
    TTF_Font *police1 = TTF_OpenFont("Graphics/fonts/nick.ttf", 50);
    SDL_Rect pos_sword, pos_coin, pos_crown, pos_castle;
    SDL_Color color = {255, 255, 255, 255};

    // TEXTE //

    int minx, maxx, miny, maxy, advance;
    if (TTF_GlyphMetrics(police, 'g', &minx, &maxx, &miny, &maxy, &advance) == -1)
        printf("%s\n", TTF_GetError());

    //RECTANGLE

    SDL_Rect grid_cursor =
        {
            .x = (grid_width - 1) / 2 * grid_cell_size,
            .y = (grid_height - 1) / 2 * grid_cell_size,
            .w = grid_cell_size,
            .h = grid_cell_size,
        };

    pos_sword.x = 0;
    pos_sword.y = 0;
    pos_sword.h = grid_cell_size;
    pos_sword.w = grid_cell_size;

    pos_castle.x = 0;
    pos_castle.y = 0;
    pos_castle.h = grid_cell_size;
    pos_castle.w = grid_cell_size;

    pos_crown.x = 0;
    pos_crown.y = 0;
    pos_crown.h = grid_cell_size;
    pos_crown.w = grid_cell_size;

    pos_coin.x = 0;
    pos_coin.y = 0;
    pos_coin.h = grid_cell_size;
    pos_coin.w = grid_cell_size;

    SDL_Color grid_background = {22, 22, 22, 255};
    SDL_Color grid_line_color = {44, 44, 44, 255};

    // TEXTURE //
    SDL_Texture *swordTex = SDL_CreateTextureFromSurface(renderer, swordS);
    SDL_Texture *castleTex = SDL_CreateTextureFromSurface(renderer, castleS);
    SDL_Texture *coinTex = SDL_CreateTextureFromSurface(renderer, coinS);
    SDL_Texture *crownTex = SDL_CreateTextureFromSurface(renderer, crownS);
    SDL_Texture *tmanant, *twarrior, *tlord, *tcursor, *tTreasure;

    SDL_Surface *warrior = NULL;
    SDL_Surface *peasant = NULL;
    SDL_Surface *lord = NULL;
    SDL_Surface *gagne = NULL;
    SDL_Surface *cursorSurface = NULL;
    SDL_Surface *treasureSurface = NULL;

    Castle *castle = NULL;
    int nchatRouge = 0, ncastleBleu = 0;
    SDL_bool _quit = SDL_FALSE, mouse_hover = SDL_FALSE, paus = SDL_FALSE, action = SDL_FALSE;
    Coordinates cood;
    int x_coord, y_coord;
    int res = 0;

    // BOUCLE D'EVENEMNT //
    while (!_quit)
    {

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_SPACE:
                    if (!paus)
                    {
                        if (PauseMenue(world, window, renderer, policeP) == -1)
                        {
                            _quit = SDL_TRUE;
                            goto end;
                        }
                    }
                    else if (paus)
                        paus = SDL_FALSE;
                case SDLK_e:
                    if (cursor)
                        cursor = SDL_FALSE;
                    else
                        cursor = SDL_TRUE;
                case SDLK_UP:
                    grid_cursor.y -= grid_cell_size;
                    break;
                case SDLK_s:
                    typeOfCharPlaying(world);
                case SDLK_DOWN:
                    grid_cursor.y += grid_cell_size;
                    break;
                case SDLK_a:
                case SDLK_LEFT:
                    grid_cursor.x -= grid_cell_size;
                    break;
                case SDLK_d:
                case SDLK_RIGHT:
                    grid_cursor.x += grid_cell_size;
                    break;
                case SDLK_ESCAPE:
                    _quit = SDL_TRUE;
                    break;
                }
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    //dis = SDL_FALSE;
                    //goto disF;

                    SDL_Event mouseclick;
                    //manque le while  SDL_GetMouseState(&x_coord,&y_coord);
                    const Uint8 *state = SDL_GetKeyboardState(NULL);
                    SDL_GetMouseState(&x_coord, &y_coord);
                    x_coord /= 100;
                    y_coord /= 100;
                    res = buttonManage(world, window, renderer, police1, x_coord, y_coord);
                    if (res != 0)
                        action = SDL_TRUE;
                    goto render;
                endrend:
                    while (SDL_PollEvent(&mouseclick))
                    {
                        Castle *cast = world->board->matrix[x_coord][y_coord].castle;
                        switch (res)
                        {
                        case -1:
                            if (mouseclick.type == SDL_MOUSEBUTTONDOWN)
                            {
                                state = SDL_GetKeyboardState(NULL);
                                if (state[SDL_SCANCODE_LALT])
                                    choose = 'b';
                                if (state[SDL_SCANCODE_LSHIFT])
                                    choose = 'r';
                                Coordinates coord = {x_coord, y_coord};
                                SDL_GetMouseState(&x_coord, &y_coord);
                                x_coord /= 100;
                                y_coord /= 100;
                                Coordinates co2 = {x_coord, y_coord};
                                int re = Mooving(world, world->board->matrix[coord.x][coord.y].neighbours->tail->army, co2, choose);
                                if (re == -2)
                                {
                                    if (world->blue->castle == NULL)
                                        win = 'r';
                                    else if(world->red->castle == NULL)
                                        win = 'b';
                                    goto winScript;
                                }
                                res = 0;
                                action = SDL_FALSE;
                                choose = '0';
                            }
                            break;
                        case -2:
                            Suicide(world, world->board->matrix[x_coord][y_coord].neighbours->tail);
                            res = 0;
                            action = SDL_FALSE;
                            break;
                        case -3:
                            Immobility(world, world->board->matrix[x_coord][y_coord].neighbours->tail);
                            action = SDL_FALSE;
                            res = 0;
                            break;
                        case -4:
                            LordToCastle(world, world->board->matrix[x_coord][y_coord].neighbours->head,
                                         world->board->matrix[x_coord][y_coord].neighbours->head->army->color);
                            res = 0;
                            action = SDL_FALSE;
                            break;
                        case 1:
                            ProdPeasant(cast, world);
                            res = 0;
                            action = SDL_FALSE;
                            break;
                        case 2:
                            ProdLord(cast, world);
                            res = 0;
                            action = SDL_FALSE;
                            break;
                        case 3:
                            ProdWarrior(cast, world);
                            res = 0;
                            action = SDL_FALSE;
                            break;
                        }
                    }
                    //dis = SDL_TRUE;
                    grid_cursor.x = (event.motion.x / grid_cell_size) * grid_cell_size;
                    grid_cursor.y = (event.motion.y / grid_cell_size) * grid_cell_size;
                    break;
                }
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_ENTER && !mouse_hover)
                    mouse_hover = SDL_TRUE;
                else if (event.window.event == SDL_WINDOWEVENT_LEAVE && mouse_hover)
                    mouse_hover = SDL_FALSE;
                break;
            case SDL_QUIT:
                _quit = SDL_TRUE;
                break;
            }
        }
    render:

        // NON PAUSE //
        SDL_RenderClear(renderer);

        if (!paus)
        {
            SDL_RenderClear(renderer);
            SDL_GetMouseState(&cood.x, &cood.y);

            // DONNÉE DES character/castleX RECUPERE TOUTES LES X FRAMES //

            for (int i = 0; i < world->board->lines; i++)
                for (int j = 0; j < world->board->rows; j++)
                {
                    castle = world->board->matrix[i][j].castle;
                    if (castle != NULL)
                    {
                        if (castle->color == 'r')
                        {
                            castleR[i][j] = 'T';
                            nchatRouge++;
                        }
                        else
                        {
                            castleB[i][j] = 'T';
                            ncastleBleu++;
                        }
                    }
                    else
                    {
                        castleR[i][j] = ' ';
                        castleB[i][j] = ' ';
                    }
                }
            if (nchatRouge == 0)
            {
                win = 'b';
                goto winScript;
            }
            if (ncastleBleu == 0)
            {
                win = 'r';
                goto winScript;
            }
            nchatRouge = 0;
            ncastleBleu = 0;

            //AFFICHAGE DU FOND//
            SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g, grid_background.b, grid_background.a);
            SDL_RenderClear(renderer);

            // AFFICHAGE DES characterS ET castleX//
            for (int i = 0; i < world->board->lines; i++)
                for (int j = 0; j < world->board->rows; j++)
                {
                    if (castleB != NULL && castleB[i][j] != ' ')
                    {
                        SDL_SetRenderDrawColor(renderer, colorBLeue.r, colorBLeue.g, colorBLeue.b, colorBLeue.a);
                        SDL_Rect square = {square.x = i * grid_cell_size, square.y = j * grid_cell_size, square.h = grid_cell_size, square.w = grid_cell_size};
                        SDL_RenderFillRect(renderer, &square);
                        pos_castle.x = square.x + 0.1 * grid_cell_size;
                        pos_castle.y = square.y + 0.1 * grid_cell_size;
                        pos_castle.h = square.h - 0.2 * grid_cell_size;
                        pos_castle.w = square.w - 0.2 * grid_cell_size;
                        SDL_RenderCopy(renderer, castleTex, NULL, &pos_castle);
                    }
                    if (castleR != NULL && castleR[i][j] == 'T')
                    {
                        SDL_SetRenderDrawColor(renderer, colorRouge.r, colorRouge.g, colorRouge.b, colorRouge.a);
                        SDL_Rect square = {square.x = i * grid_cell_size, square.y = j * grid_cell_size, square.h = grid_cell_size, square.w = grid_cell_size};
                        SDL_RenderFillRect(renderer, &square);
                        pos_castle.x = square.x + 0.1 * grid_cell_size;
                        pos_castle.y = square.y + 0.1 * grid_cell_size;
                        pos_castle.h = square.h - 0.2 * grid_cell_size;
                        pos_castle.w = square.w - 0.2 * grid_cell_size;
                        SDL_RenderCopy(renderer, castleTex, NULL, &pos_castle);
                    }
                }

            SDL_SetRenderDrawColor(renderer, grid_line_color.r, grid_line_color.g, grid_line_color.b, grid_line_color.a);

            //GRILLE//
            SDL_SetRenderDrawColor(renderer, 125, 125, 125, 100);
            for (int x = 0; x < 1 + grid_width * grid_cell_size; x += grid_cell_size)
                SDL_RenderDrawLine(renderer, x, 0, x, window_height);

            for (int y = 0; y < 1 + grid_height * grid_cell_size; y += grid_cell_size)
                SDL_RenderDrawLine(renderer, 0, y, window_height, y);

            SDL_RenderCopy(renderer, castleTex, NULL, &pos_castle);

            SDL_GetMouseState(&cood.x, &cood.y);

            // CharacterS //
            for (int i = 0; i < world->board->lines; i++)
                for (int j = 0; j < world->board->rows; j++)
                {
                    VertexArmy *mail = malloc(sizeof(VertexArmy));
                    if ((mail = world->board->matrix[i][j].neighbours->head) == NULL)
                        continue;
                    if (mail->army->color == 'b')
                        SDL_SetRenderDrawColor(renderer, colorBLeue.r, colorBLeue.g, colorBLeue.b, colorBLeue.a);
                    if (mail->army->color == 'r')
                        SDL_SetRenderDrawColor(renderer, colorRouge.r, colorRouge.g, colorRouge.b, colorRouge.a);
                    SDL_Rect square = {square.x = mail->army->coord.x * grid_cell_size, square.y = mail->army->coord.y * grid_cell_size, square.h = grid_cell_size, square.w = grid_cell_size};
                    SDL_bool occupe = SDL_TRUE;
                    while (mail->next != NULL)
                    {
                        if (mail->army->task == '0')
                            occupe = SDL_FALSE;
                        switch (mail->army->type)
                        {
                        case Lord:
                            nbUnite[i][j][0]++;
                            break;
                        case Warrior:
                            nbUnite[i][j][1]++;
                            break;
                        case Peasant:
                            nbUnite[i][j][2]++;
                            break;
                        }
                        mail = mail->next;
                    }
                    if (mail->next == NULL)
                    {
                        SDL_RenderFillRect(renderer, &square);

                        if (mail->army->task == '0')
                            occupe = SDL_FALSE;
                        if (mail->army->type == Lord)
                        {
                            pos_crown.x = i * grid_cell_size + 0.1 * grid_cell_size;
                            pos_crown.y = j * grid_cell_size + 0.1 * grid_cell_size;
                            pos_crown.h = grid_cell_size - 0.2 * grid_cell_size;
                            pos_crown.w = grid_cell_size - 0.2 * grid_cell_size;
                            SDL_SetRenderDrawColor(renderer, 25, 25, 25, 100);
                            SDL_RenderCopy(renderer, crownTex, NULL, &pos_crown);
                            nbUnite[i][j][0]++;
                        }
                        if (mail->army->type == Warrior)
                        {
                            pos_sword.x = i * grid_cell_size + 0.1 * grid_cell_size;
                            pos_sword.y = j * grid_cell_size + 0.1 * grid_cell_size;
                            pos_sword.h = grid_cell_size - 0.2 * grid_cell_size;
                            pos_sword.w = grid_cell_size - 0.2 * grid_cell_size;
                            SDL_SetRenderDrawColor(renderer, 25, 25, 25, 100);
                            SDL_RenderCopy(renderer, swordTex, NULL, &pos_sword);
                            nbUnite[i][j][1]++;
                        }

                        if (mail->army->type == Peasant)
                        {
                            pos_coin.x = i * grid_cell_size + 0.1 * grid_cell_size;
                            pos_coin.y = j * grid_cell_size + 0.1 * grid_cell_size;
                            pos_coin.h = grid_cell_size - 0.2 * grid_cell_size;
                            pos_coin.w = grid_cell_size - 0.2 * grid_cell_size;
                            SDL_SetRenderDrawColor(renderer, 25, 25, 25, 100);
                            SDL_RenderCopy(renderer, coinTex, NULL, &pos_coin);
                            nbUnite[i][j][2]++;
                        }
                        SDL_SetRenderDrawColor(renderer, 125, 125, 125, 100);
                        SDL_RenderDrawLine(renderer, i * grid_cell_size, j * grid_cell_size, (i + 1) * grid_cell_size, j * grid_cell_size);
                        SDL_RenderDrawLine(renderer, i * grid_cell_size, (j + 1) * grid_cell_size, (i + 1) * grid_cell_size, (j + 1) * grid_cell_size);
                        SDL_RenderDrawLine(renderer, i * grid_cell_size, j * grid_cell_size, i * grid_cell_size, (j + 1) * grid_cell_size);
                        SDL_RenderDrawLine(renderer, (i + 1) * grid_cell_size, j * grid_cell_size, (i + 1) * grid_cell_size, (j + 1) * grid_cell_size);
                    }
                    if (!occupe)
                        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 0);
                    else
                        SDL_SetRenderDrawColor(renderer, colorRouge.r, colorRouge.g, colorRouge.b, colorRouge.a);

                    if (mail->army->color == world->turn)
                    {
                        SDL_RenderDrawLine(renderer, i * grid_cell_size, j * grid_cell_size, (i + 1) * grid_cell_size, j * grid_cell_size);
                        SDL_RenderDrawLine(renderer, i * grid_cell_size, (j + 1) * grid_cell_size, (i + 1) * grid_cell_size, (j + 1) * grid_cell_size);
                        SDL_RenderDrawLine(renderer, i * grid_cell_size, j * grid_cell_size, i * grid_cell_size, (j + 1) * grid_cell_size);
                        SDL_RenderDrawLine(renderer, (i + 1) * grid_cell_size, j * grid_cell_size, (i + 1) * grid_cell_size, (j + 1) * grid_cell_size);
                    }
                    SDL_SetRenderDrawColor(renderer, 125, 125, 125, 100);
                    //CURSEUR//
                }
            // castle //
            if (cursor)
            {
                char *st = malloc(T_MAX * sizeof(char));
                SDL_Rect square;

                Castle *chat = world->board->matrix[cood.x / 100][cood.y / 100].castle;
                if (chat != NULL)
                {
                    if (chat->color == 'r')
                        sprintf(st, "Treasure %d", world->red->treasure);
                    else
                        sprintf(st, "Treasure %d", world->blue->treasure);
                    treasureSurface = TTF_RenderText_Blended(police, st, color);
                    sprintf(st, "Task : %c ", chat->task);
                    cursorSurface = TTF_RenderText_Blended(police, st, color);
                    if (cood.x + square.w > WINSIZE)
                        square.x = cood.x - square.w;
                    else
                        square.x = cood.x;
                    if (cood.y + 2 * (maxy - miny) > WINSIZE)
                        square.y = cood.y - 2 * (maxy - miny);
                    else
                        square.y = cood.y;
                    tcursor = SDL_CreateTextureFromSurface(renderer, cursorSurface);
                    tTreasure = SDL_CreateTextureFromSurface(renderer, treasureSurface);
                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 200);
                    square.h = grid_cell_size / 4 + maxy - miny;
                    SDL_RenderFillRect(renderer, &square);
                    square.h = grid_cell_size / 4;
                    SDL_SetRenderDrawColor(renderer, 25, 25, 25, 100);
                    SDL_RenderCopy(renderer, tTreasure, NULL, &square);
                    square.y += maxy - miny;
                    SDL_RenderCopy(renderer, tcursor, NULL, &square);
                }
                SDL_GetMouseState(&cood.x, &cood.y);
                sprintf(st, "Warrior : %d ", nbUnite[cood.x / 100][cood.y / 100][1]);
                warrior = TTF_RenderText_Blended(police, st, color);
                sprintf(st, "Peasant : %d ", nbUnite[cood.x / 100][cood.y / 100][2]);
                peasant = TTF_RenderText_Blended(police, st, color);
                sprintf(st, "Lord : %d ", nbUnite[cood.x / 100][cood.y / 100][0]);
                lord = TTF_RenderText_Blended(police, st, color);
                VertexArmy *character = world->board->matrix[cood.x / 100][cood.y / 100].neighbours->tail;
                if (character != NULL && character->army != NULL)
                {
                    if (cood.x + square.w > WINSIZE)
                        square.x = cood.x - square.w;
                    else
                        square.x = cood.x;
                    if (cood.y + 3 * (maxy - miny) > WINSIZE)
                        square.y = cood.y - 3 * (maxy - miny);
                    else
                        square.y = cood.y;
                    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 200);
                    square.h = grid_cell_size / 4 + 2 * (maxy - miny);
                    SDL_RenderFillRect(renderer, &square);
                    square.h = grid_cell_size / 4;
                    SDL_SetRenderDrawColor(renderer, 25, 25, 25, 100);

                    tlord = SDL_CreateTextureFromSurface(renderer, lord);
                    SDL_SetRenderDrawColor(renderer, colorBlanche.r, colorBlanche.g, colorBlanche.b, colorBlanche.a);
                    SDL_RenderCopy(renderer, tlord, NULL, &square);
                    square.y += maxy - miny;
                    twarrior = SDL_CreateTextureFromSurface(renderer, warrior);
                    SDL_RenderCopy(renderer, twarrior, NULL, &square);
                    square.y += maxy - miny;
                    tmanant = SDL_CreateTextureFromSurface(renderer, peasant);
                    SDL_RenderCopy(renderer, tmanant, NULL, &square);
                }
                if (treasureSurface != NULL)
                    SDL_FreeSurface(treasureSurface);
                if (cursorSurface != NULL)
                    SDL_FreeSurface(cursorSurface);
                SDL_FreeSurface(warrior);
                SDL_FreeSurface(lord);
            }
        }
        for (int i = 0; i < ligne * colonne; i++)
            for (int j = 0; j < 3; j++)
                nbUnite[i / ligne][i % ligne][j] = 0;

        //SDL_SetRenderTarget(renderer, )
        SDL_RenderPresent(renderer);

        SDL_Event equit;
        if (action)
            goto endrend;
    winScript:
        if (win == 'r')
        {
            scoreManage(world);
            SDL_RenderClear(renderer);
            SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g, grid_background.b, grid_background.a);
            SDL_Rect winS = {0, 0, WINSIZE, WINSIZE};
            SDL_RenderFillRect(renderer, &winS);
            sprintf(st, "ROUGE GAGNE");
            gagne = TTF_RenderText_Blended(police1, st, colorRouge);
            SDL_Rect square = {WINSIZE / 2 - gagne->w / 2, WINSIZE / 2 - gagne->h / 2, gagne->w, gagne->h};
            SDL_Texture *tgagne = SDL_CreateTextureFromSurface(renderer, gagne);
            SDL_RenderCopy(renderer, tgagne, NULL, &square);
            SDL_RenderPresent(renderer);

            while (SDL_WaitEvent(&equit))
            {
                if (equit.key.keysym.sym == SDLK_ESCAPE)
                    goto end;
            }
        }
        if (win == 'b')
        {
            scoreManage(world);
            SDL_RenderClear(renderer);
            SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g, grid_background.b, grid_background.a);
            SDL_Rect winS = {0, 0, WINSIZE, WINSIZE};
            SDL_RenderFillRect(renderer, &winS);
            sprintf(st, "BLEU GAGNE");
            gagne = TTF_RenderText_Blended(police1, st, colorBLeue);
            SDL_Rect square = {WINSIZE / 2 - gagne->w / 2, WINSIZE / 2 - gagne->h / 2, gagne->w, gagne->h};
            SDL_Texture *tgagne = SDL_CreateTextureFromSurface(renderer, gagne);
            SDL_RenderCopy(renderer, tgagne, NULL, &square);
            SDL_RenderPresent(renderer);
            while (SDL_WaitEvent(&equit))
            {
                if (equit.key.keysym.sym == SDLK_ESCAPE)
                    goto end;
            }
        }
    }
//LIBERATION DES DONNES//
end:
    TTF_CloseFont(police);
    if (swordS != NULL)
        SDL_FreeSurface(swordS);
    if (castleS != NULL)
        SDL_FreeSurface(castleS);
    if (crownS != NULL)
        SDL_FreeSurface(crownS);
    if (treasureSurface != NULL)
        SDL_FreeSurface(treasureSurface);
    if (cursorSurface != NULL)
        SDL_FreeSurface(cursorSurface);
    if (lord != NULL)
        SDL_FreeSurface(lord);
    if (warrior != NULL)
        SDL_FreeSurface(warrior);
    if (peasant != NULL)
        SDL_FreeSurface(peasant);
    if (coinS != NULL)
        SDL_FreeSurface(coinS);
    if (gagne != NULL)
        SDL_FreeSurface(gagne);

    return EXIT_SUCCESS;
}

int PauseMenue(World *world, SDL_Window *window, SDL_Renderer *renderer, TTF_Font *police)
{

    SDL_Rect win = {WINSIZE / 4, WINSIZE / 12, WINSIZE / 2, WINSIZE * 0.8};
    int maxy, maxx, miny, minx, advance;
    if (TTF_GlyphMetrics(police, 'Q', &minx, &maxx, &miny, &maxy, &advance) == -1)
        printf("%s\n", TTF_GetError());

    SDL_Rect square1 = {win.x + 0.25 * win.w, win.y + 0.2 * WINSIZE, win.w / 2, win.h / 10};
    SDL_Rect square2 = {win.x + 0.25 * win.w, win.y + 0.3 * WINSIZE, win.w / 2, win.h / 10};
    SDL_Rect square3 = {win.x + 0.25 * win.w, win.y + win.h - (maxy - miny) - 50, win.w / 2, win.h / 10};
    SDL_Rect square4 = {win.x + 0.25 * win.w, win.y + 50, win.w / 2, win.h / 10};

    SDL_SetRenderDrawColor(renderer, 44, 44, 44, 255);

    SDL_RenderFillRect(renderer, &win);

    SDL_Color color = {255, 255, 255, 255};
    char *st = malloc(T_MAX * sizeof(char));

    sprintf(st, "Reprendre");

    SDL_Surface *resume = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tresume = SDL_CreateTextureFromSurface(renderer, resume);
    square1.w = resume->w;
    square1.x = win.x + (win.w - resume->w) / 2;
    square1.h = resume->h;
    SDL_RenderCopy(renderer, tresume, NULL, &square1);

    sprintf(st, "Sauvegarder");
    SDL_Surface *save = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tsave = SDL_CreateTextureFromSurface(renderer, save);
    square2.h = save->h;
    square2.w = save->w;
    square2.x = win.x + (win.w - save->w) / 2;
    SDL_RenderCopy(renderer, tsave, NULL, &square2);

    sprintf(st, "Quitter");
    SDL_Surface *quit = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tquit = SDL_CreateTextureFromSurface(renderer, quit);
    square3.h = quit->h;
    square3.x = win.x + (win.w - quit->w) / 2;
    square3.w = quit->w;
    SDL_RenderCopy(renderer, tquit, NULL, &square3);

    sprintf(st, "Pause");

    SDL_Color colorJaune = {255, 255, 0, 255};

    SDL_Surface *paus = TTF_RenderText_Blended(police, st, color);
    SDL_Texture *tpaus = SDL_CreateTextureFromSurface(renderer, paus);
    square4.w = paus->w;
    square4.x = win.x + (win.w - paus->w) / 2;
    square4.h = paus->h;
    SDL_RenderCopy(renderer, tpaus, NULL, &square4);

    SDL_bool _quit = SDL_FALSE, qT = SDL_FALSE;
    SDL_Event event;

    Coordinates cood;
    while (!_quit)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE || event.key.keysym.sym == SDLK_SPACE)
                {
                    _quit = SDL_TRUE;
                    break;
                }
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&cood.x, &cood.y);
                if (cood.x > square1.x && cood.x < square1.x + square1.w)
                {
                    if (cood.y > square1.y && cood.y < square1.y + square1.h)
                    {
                        resume = TTF_RenderText_Blended(police,
                                                           "Reprendre", colorJaune);
                        tresume = SDL_CreateTextureFromSurface(renderer, resume);
                    }
                    else
                    {
                        resume = TTF_RenderText_Blended(police,
                                                           "Reprendre", color);
                        tresume = SDL_CreateTextureFromSurface(renderer, resume);
                    }
                    if (cood.y > square2.y && cood.y < square2.y + square2.h)
                    {
                        save = TTF_RenderText_Blended(police,
                                                             "Sauvegarder", colorJaune);
                        tsave = SDL_CreateTextureFromSurface(renderer, save);
                    }
                    else
                    {
                        save = TTF_RenderText_Blended(police,
                                                             "Sauvegarder", color);
                        tsave = SDL_CreateTextureFromSurface(renderer, save);
                    }
                    if (cood.y > square3.y && cood.y < square3.y + square3.h)
                    {
                        quit = TTF_RenderText_Blended(police,
                                                         "Quitter", colorJaune);
                        tquit = SDL_CreateTextureFromSurface(renderer, quit);
                    }
                    else
                    {
                        quit = TTF_RenderText_Blended(police,
                                                         "Quitter", color);
                        tquit = SDL_CreateTextureFromSurface(renderer, quit);
                    }
                }
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&cood.x, &cood.y);

                if (cood.x > square1.x && cood.x < square1.x + square1.w && event.button.button == SDL_BUTTON_LEFT)
                {
                    if (cood.y > square1.y && cood.y < square1.y + square1.h)
                        _quit = SDL_TRUE;
                    else if (cood.y > square2.y && cood.y < square2.y + square2.h)
                    {
                        fileManage(world);
                        _quit = SDL_TRUE;
                    }
                    else if (cood.y > square3.y && cood.y < square3.y + square3.h)
                    {
                        _quit = SDL_TRUE;
                        qT = SDL_TRUE;
                    }
                }
            }
        }
        SDL_RenderFillRect(renderer, &win);
        SDL_RenderCopy(renderer, tresume, NULL, &square1);
        SDL_RenderCopy(renderer, tsave, NULL, &square2);
        SDL_RenderCopy(renderer, tquit, NULL, &square3);
        SDL_RenderCopy(renderer, tpaus, NULL, &square4);
        SDL_RenderPresent(renderer);
    }
    free(st);
    SDL_FreeSurface(resume);
    SDL_FreeSurface(save);
    SDL_FreeSurface(paus);
    SDL_FreeSurface(quit);
    if (!qT)
        return 0;
    return -1;
}

void DisplayMenue(World *world)
{
    WINSIZE = world->board->lines * 100;
    SDL_Init(SDL_INIT_EVERYTHING);
    TTF_Init();
    IMG_Init(IMG_INIT_PNG);
    SDL_Color colorBlanche = {255, 255, 255, 255};
    TTF_Font *police = TTF_OpenFont("Graphics/fonts/nick.ttf", 50);
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Surface *surfaceN = NULL, *surfaceC = NULL, *surfaceQ = NULL;
    surfaceN = TTF_RenderText_Blended(police,
                                      "Nouvelle Partie", colorBlanche);
    surfaceC = TTF_RenderText_Blended(police,
                                      "Charger", colorBlanche);
    surfaceQ = TTF_RenderText_Blended(police,
                                      "Quitter", colorBlanche);
    SDL_Color colorJaune = {255, 255, 0, 255};
    SDL_SetWindowTitle(window, "Mido-Age");
    //SDL_CreateWindowAndRenderer(WINSIZE, WINSIZE, 0, &window, &renderer);
#ifdef __linux__
    window = SDL_CreateWindow("Mido Age", 0, 0, WINSIZE, WINSIZE, 0);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
#endif
#ifndef __linux__
    SDL_CreateWindowAndRenderer(WINSIZE, WINSIZE, SDL_RENDERER_ACCELERATED, &window, &renderer);
#endif
    SDL_Color grid_background = {22, 22, 22, 255};
    SDL_bool _quit = SDL_FALSE;
    SDL_Rect startButton, chargeButton, quitButton;
    int bordX = (WINSIZE - surfaceN->w) / 2;
    SDL_Surface *icon = IMG_Load("Graphics/icons/got.png");
    //SDL_SetWindowIcon(window,icon);

    // The icon is attached to the window pointer
    SDL_SetWindowIcon(window, icon);

    startButton.x = (WINSIZE - surfaceN->w) / 2;
    startButton.y = 150;
    startButton.w = surfaceN->w;
    startButton.h = surfaceN->h;

    chargeButton.x = (WINSIZE - surfaceC->w) / 2;
    chargeButton.y = 300;
    chargeButton.w = surfaceC->w;
    chargeButton.h = surfaceC->h;

    quitButton.x = (WINSIZE - surfaceC->w) / 2;
    quitButton.y = 450;
    quitButton.w = surfaceC->w;
    quitButton.h = surfaceC->h;

    SDL_Texture *textureN = SDL_CreateTextureFromSurface(renderer,
                                                         surfaceN);

    SDL_Texture *textureC = SDL_CreateTextureFromSurface(renderer,
                                                         surfaceC);

    SDL_Texture *textureQ = SDL_CreateTextureFromSurface(renderer, surfaceQ);

    Coordinates cood;
    while (!_quit)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_MOUSEBUTTONDOWN:
                SDL_GetMouseState(&cood.x, &cood.y);
                if (cood.x > startButton.x && cood.x < WINSIZE - bordX)
                {
                    if (cood.y > startButton.y && cood.y < startButton.y + startButton.h)
                    {
                        windowsManage(world, renderer, window);
                        _quit = SDL_TRUE;
                        goto end;
                    }
                    if (cood.y > chargeButton.y && cood.y < chargeButton.y + chargeButton.h)
                    {
                        world = loadGame();
                        windowsManage(world, renderer, window);
                        goto end;
                    }
                    if (cood.y > quitButton.y && cood.y < quitButton.y + quitButton.h)
                        _quit = SDL_TRUE;
                }
                break;
            case SDL_MOUSEMOTION:
                SDL_GetMouseState(&cood.x, &cood.y);
                if (cood.x > startButton.x && cood.x < WINSIZE - bordX)
                {
                    if (cood.y > startButton.y && cood.y < startButton.y + startButton.h)
                    {
                        surfaceN = TTF_RenderText_Blended(police,
                                                          "Nouvelle Partie", colorJaune);
                        textureN = SDL_CreateTextureFromSurface(renderer, surfaceN);
                    }
                    else
                    {
                        surfaceN = TTF_RenderText_Blended(police,
                                                          "Nouvelle Partie", colorBlanche);
                        textureN = SDL_CreateTextureFromSurface(renderer, surfaceN);
                    }
                    if (cood.y > chargeButton.y && cood.y < chargeButton.y + chargeButton.h)
                    {
                        surfaceC = TTF_RenderText_Blended(police,
                                                          "Charger", colorJaune);
                        textureC = SDL_CreateTextureFromSurface(renderer, surfaceC);
                    }
                    else
                    {
                        surfaceC = TTF_RenderText_Blended(police,
                                                          "Charger", colorBlanche);
                        textureC = SDL_CreateTextureFromSurface(renderer, surfaceC);
                    }
                    if (cood.y > quitButton.y && cood.y < quitButton.y + quitButton.h)
                    {
                        surfaceQ = TTF_RenderText_Blended(police,
                                                          "Quitter", colorJaune);
                        textureQ = SDL_CreateTextureFromSurface(renderer, surfaceQ);
                    }
                    else
                    {
                        surfaceQ = TTF_RenderText_Blended(police,
                                                          "Quitter", colorBlanche);
                        textureQ = SDL_CreateTextureFromSurface(renderer, surfaceQ);
                    }
                }
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE)
                    _quit = SDL_TRUE;
            }
            SDL_RenderClear(renderer);
            SDL_SetRenderDrawColor(renderer, grid_background.r, grid_background.g, grid_background.b, grid_background.a);
            SDL_RenderPresent(renderer);
            SDL_RenderCopy(renderer, textureN, NULL, &startButton);
            SDL_RenderCopy(renderer, textureC, NULL, &chargeButton);
            SDL_RenderCopy(renderer, textureQ, NULL, &quitButton);
            SDL_RenderPresent(renderer);
        }
    }
end:
    FreeWorld(world);
    SDL_FreeSurface(surfaceN);
    SDL_FreeSurface(surfaceQ);
    SDL_DestroyTexture(textureN);
    SDL_FreeSurface(surfaceC);
    SDL_DestroyTexture(textureC);
    SDL_DestroyTexture(textureQ);
    TTF_Quit();
    IMG_Quit();
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
}
