var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstvwxy",
  1: "clpv",
  2: "cdefgilmrsw",
  3: "abcdefgilmnprstvwxy",
  4: "acdehnopstvwxy",
  5: "clptv",
  6: "t",
  7: "lpw",
  8: "bcdegistw",
  9: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

