# Compilation et Manuel

Rapide manuel d'utilisation de mido-age pour linux et windows.

## Compilation

L'outil de compilation utilisé est make, pour les ordinateurs sous linux, trois librairies sont à télécharger via la commande :

```bash
sudo apt-get install libsdl2-dev
```
Les bibilothèques liées à l'affichage text SDL_TTF et des images SDL_IMG sont prises en charge dans ce paquet.
Pour plus de détails vous pouvez consulter :

 [SDL2](https://packages.ubuntu.com/search?keywords=sdl2)

Pour les versions Windows, il est nécessaire d'installer les librairies associées, une attention doit être apportée à la version choisie qui doit être en adéquation avec votre architecture/compilateur.

[SDL2](https://www.libsdl.org/download-2.0.php)\
[SDL2_TTF](https://www.libsdl.org/projects/SDL_ttf/)\
[SDL2_IMG](https://www.libsdl.org/projects/SDL_image/)
## Manuel

Une fois le projet lancé et la partie générée il existe quelques commandes :

* Le changement de tour :

S'effectue avec la touche **s** et (les chateaux pouvant être inactifs)

* Le choix de l'issue: 

Lors du déplacement restez appuyé sur la touche **ctrl left** pour forcer la victoire des bleus ou **shift left** pour les unités rouges.

* L'affichage des informations (expérimental) :

Pressez la touche **e**, attention dès que l'information est intégrée réappuyer avant de changer de tour pour éviter un potentiel plantage.

### La pause
Pour sauvegarder ou simplement arrêter la partie en cours appuyer sur **espace** pour afficher le menu pause.

### Fin de partie 

A la fin de votre partie les scores sont calculés et présentés dans le fichier score.txt pour quitter la partie appuyez sur **escape**.

## Documentation

La documentation a été fournie par *Doxygen* pour la version complète ouvrez le fichier index.html avec votre navigateur ('Documentation/html/index.html) 
les graphs sont également fournis au format pdf dans le repertoire Latex, avec leur .tex associé.

## Teams

Victor GLASER : [gitlab](https://gitlab.com/glaservictor)\
David PREMACHANDRA
