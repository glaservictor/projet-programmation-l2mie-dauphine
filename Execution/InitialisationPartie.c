#include "Exec.h"
#include "ListChainedHeritageCastle.h"

void AddChar(char color, World *world, Type type, Coordinates coord, Coordinates dest, Castle *castle)
{
    Characters *character;
    switch (type)
    {
    case Lord:
        character = CreateLord(castle, color, coord.x, coord.y)->army;
        break;
    case Peasant:
        character = CreatePeasant(castle, color, coord.x, coord.y)->army;
        break;
    case Warrior:
        character = CreateWarrior(castle, color, coord.x, coord.y)->army;
        break;
    }
    character->destination = dest;
    if(coord.x != dest.x || coord.y != dest.y)
    {
        character->task = '1';
        if (dest.x == -1)
            character->order = 'i';
        else 
            character->order = 'd';
    }
    VertexArmy *vertex = VertexInitializationArmy(character);
    AddTailArmy(world->board->matrix[coord.x][coord.y].neighbours, vertex);
}

World *InitialisationPartie(int size)
{
    Coordinates coords = {0, 0};
    World *world = InitWorld(size, size);
    int colonne = world->board->rows - 1, ligne = world->board->lines - 1;
    Coordinates coord = {colonne, ligne}, coordRG = {1, 0}, coordRM = {0, 1}, coordBG = {colonne - 1, ligne}, coordMG = {colonne, ligne - 1};
    Castle *castle = InitCastle(1, 'r', coords), *chateauB = InitCastle(1, 'b', coord);
    AddChar('r', world, Peasant, coordRM, coordRM, castle);
    AddChar('r', world, Lord, coordRG, coordRG, castle);
    AddChar('b', world, Lord, coordBG, coordBG, chateauB);
    AddChar('b', world, Peasant, coordMG, coordMG, chateauB);
    VertexCastle *MaillonChateauB = VertexInitCastle(chateauB);
    VertexCastle *VertexCastle = VertexInitCastle(castle);
    AddHeadCastle(world->red->castle, VertexCastle);
    AddHeadCastle(world->blue->castle, MaillonChateauB);
    Coordinates co;
    co.x = world->blue->castle->head->castle->coord.x;
    co.y = world->blue->castle->head->castle->coord.y;
    world->board->matrix[co.x][co.y].castle = world->blue->castle->head->castle;
    co = world->red->castle->head->castle->coord;
    world->board->matrix[co.x][co.y].castle = world->red->castle->head->castle;
    return world;
}
