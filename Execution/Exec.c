#include "Exec.h"

VertexCastle *RemainCastlePlaying(ListDChainedCastle *list)
{
    int n = list->size;
    VertexCastle *tmp = list->head;
    for (int i = 0; i < n; i++)
    {
        if (tmp->castle->task == '0')
        {
            return tmp;
        }
        else
            tmp = tmp->next;
    }
    return NULL;
}

VertexArmy *RemainCharPlaying(Castle *castle, int typ)
{
    ListDChainedArmy *list = castle->elements;
    VertexArmy *tmp = list->tail;
    while (tmp != NULL && TypeToInt(tmp->army->type) >= typ)
    { // break si on a pas trouvé le bon type
        if (tmp->army->task == '0')
            return tmp; // si la task n'est pas à 0, alors le characters n'a pas joué.
        else
            tmp = tmp->prev;
    }
    return NULL;
}

void SortListCharOcc(ListDChainedArmy *list)
{
    if (list->size < 2)
        return;
    VertexArmy *tmp = list->head;
    for (int i = 0; i < list->size; i++)
    {
        if (tmp != NULL && tmp->army->task == '0')
        {
            VertexArmy *next = tmp->next;
            AddTailArmy(list, DeleteListArmy(list, i));
            tmp = next;
        }
        else if (tmp != NULL)
            tmp = tmp->next;
    }
}

void SortListCharType(ListDChainedArmy *list)
{
    int index = 0;
    if (list->size < 2)
        return;
    VertexArmy *tmp = list->head;
    for (int i = 0; i < 3; i++)
    {
        while (tmp != NULL)
        {
            if (TypeToInt(tmp->army->type) == i)
                AddTailArmy(list, DeleteListArmy(list, index));
            tmp = tmp->next;
            index++;
        }
        tmp = list->head;
        index = 0;
    }
    SortListCharOcc(list);
}

void AddOrder(VertexArmy *character, char order)
{
    character->army->order = order;
    character->army->task = '1';
}

VertexCastle *FindCastleOfChar(VertexArmy *character, World *world)
{
    char color = character->army->color;
    VertexCastle *tmpc = malloc(sizeof(VertexCastle));
    int size = 0;
    if (color == 'r')
    {
        tmpc = world->red->castle->head;
        size = world->red->castle->size;
    }
    else
    {
        tmpc = world->blue->castle->head;
        size = world->blue->castle->size;
    }
    for (int i = 0; i < size; i++)
    {
        int taille = tmpc->castle->elements->size;
        VertexArmy *tmpa = tmpc->castle->elements->head;
        int j = 0;
        while (j < taille)
        {
            if (tmpa != NULL && tmpa->army == character->army)
                return tmpc;
            tmpa = tmpa->next;
            j++;
        }
        tmpc = tmpc->next;
    }
    return NULL;
}

int FindOutIndexChar(ListDChainedArmy *list, VertexArmy *character)
{
    if (list->size == 0)
        return -1;
    int n = 0;
    VertexArmy *tmp = list->head;
    while (tmp->army != character->army)
    {
        n++;
        tmp = tmp->next;
    }
    return n;
}

VertexArmy *FightChar(World *world, VertexArmy *character1, VertexArmy *character2, char choose)
{
    Type type1 = TypeToInt(character1->army->type);
    Type type2 = TypeToInt(character2->army->type);
    char color1 = character1->army->color;
    int val1;
    int val2;
    switch (type1)
    {
    case 0:
        val1 = 20;
        break;
    case 1:
        val1 = 5;
        break;
    case 2:
        val1 = 1;
        break;
    default:
        break;
    }
    switch (type2)
    {
    case 0:
        val2 = 20;
        break;
    case 1:
        val2 = 5;
        break;
    case 2:
        val2 = 1;
        break;
    default:
        break;
    }
    int tot = val1 + val2;
    int res = 0;

    if (choose == color1)
        res = 0;
    else if (choose != '0')
        res = val1;
    else
        res = rand() % tot;

    ListDChainedArmy *listenei = world->board->matrix[character1->army->coord.x][character1->army->coord.y].neighbours;
    SortListCharType(listenei);
    if (res < val1)
    { /*le character1 a gagné on supprime le character2*/
        int n = FindOutIndexChar(listenei, character2);
        DeleteListArmy(listenei, n);
        VertexCastle *tmpc = FindCastleOfChar(character2, world);
        int j = FindOutIndexChar(tmpc->castle->elements, character2);
        DeleteListArmy(tmpc->castle->elements, j);
        VertexArmy *del = character2;
        character2 = NULL;
        FreeVertexArmy(del);
        return character1;
    }
    else
    {
        int n = FindOutIndexChar(listenei, character1);
        DeleteListArmy(listenei, n);
        VertexCastle *tmpc = FindCastleOfChar(character1, world);
        int j = FindOutIndexChar(tmpc->castle->elements, character1);
        DeleteListArmy(tmpc->castle->elements, j);
        VertexArmy *del = character1;
        character1 = NULL;
        FreeVertexArmy(del);
        return character2;
    }
}

int FindIndexCastle(World *world, Castle *c)
{
    int n = 0;
    if (c->color == 'r')
    {
        ListDChainedCastle *list = world->red->castle;
        VertexCastle *tmp = list->head;
        while (tmp->castle != c)
        {
            n++;
            tmp = tmp->next;
        }
    }
    else
    {
        ListDChainedCastle *list = world->blue->castle;
        VertexCastle *tmp = list->head;
        while (tmp->castle != c)
        {
            n++;
            tmp = tmp->next;
        }
    }
    return n;
}

void AddHeadArmyWithVertexWithoutPointer(ListDChainedArmy *list, VertexArmy vertex)
{
    VertexArmy *head = list->head;
    vertex.prev = NULL;
    if (list->head == NULL)
    {
        list->head = &vertex;
        list->tail = &vertex;
        vertex.next = NULL;
    }
    else
    {
        head->prev = &vertex;
        vertex.next = head;
        list->head = &vertex;
    }
    list->size++;
}

ListDChainedArmy *AllegoryChangePeasants(VertexArmy *character, Castle *castle, World *world)
{

    ListDChainedArmy *list1 = castle->elements;
    SortListCharType(list1);
    VertexCastle *newcastle = FindCastleOfChar(character, world);
    ListDChainedArmy *list2 = newcastle->castle->elements;
    ListDChainedArmy *newlist = ListInitializationArmy();
    SortListCharType(list2);
    VertexArmy *tmp = list1->tail;
    int n = list1->size - 1;
    while (n >= 0 && TypeToInt(tmp->army->type) == 2)
    {
        tmp->army->color = character->army->color;
        tmp = tmp->prev;
        AddTailArmy(list2, DeleteListArmy(list1, n));
        n--;
        VertexArmy *newPeas = VertexInitializationArmy(list2->tail->army);        
        AddHeadArmy(newlist, newPeas);
    }
    return newlist;
}

void PeasantFight(ListDChainedArmy *list, World *world)
{
    int i = list->size;
    VertexArmy *tmp = list->head;
    while (i > 0)
    {
        ListDChainedArmy *lennemy = world->board->matrix[tmp->army->coord.x][tmp->army->coord.y].neighbours;
        SortListCharType(lennemy);
        VertexArmy *ennemy = lennemy->tail;
        int sizeEnnemy = lennemy->size;
        while (tmp != NULL && sizeEnnemy > 0)
        {
            if (tmp->army->color == ennemy->army->color)
            {
                ennemy = ennemy->prev;
                sizeEnnemy--;
            }
            else
            {
                if (ennemy == FightChar(world, tmp, ennemy, '0'))
                    PeasantFight(list, world);
                else
                    sizeEnnemy--;
            }
        }
        tmp = tmp->next;
        i--;
    }
}

void DeleteCastle(World *world, Castle *castle, VertexArmy *character)
{
    int x = castle->coord.x;
    int y = castle->coord.y;
    world->board->matrix[x][y].castle = NULL;
    PeasantFight(AllegoryChangePeasants(character, castle, world), world);
    int n = castle->elements->size;
    VertexArmy *tmp = castle->elements->head;
    for (int i = 0; i < n; i++)
    {
        VertexArmy *prev = tmp;
        tmp = tmp->next;
        free(prev);
    }
    int k = FindIndexCastle(world, castle);
    if (castle->color == 'r')
    {
        ListDChainedCastle *list = world->red->castle;
        DeleteListCastle(list, k);
    }
    else
    {
        ListDChainedCastle *list = world->blue->castle;
        DeleteListCastle(list, k);
    }
    FreeCastle(castle);
}

Castle *FightCastle(World *world, VertexArmy *character, Castle *castle, char choose)
{
    int type = TypeToInt(character->army->type), val;
    char color = castle->color;
    switch (type)
    {
    case 0:
        val = 20;
        break;
    case 1:
        val = 5;
        break;
    case 2:
        val = 1;
        break;
    default:
        break;
    }
    int tot = val + 30;
    int res = 0;

    if (choose == color)
        res = 0;
    else if (choose != '0')
        res = 30;
    else
        res = rand() % tot;

    ListDChainedArmy *listenei = world->board->matrix[character->army->coord.x][character->army->coord.y].neighbours;
    SortListCharType(listenei);
    if (res < 30)
    { /*le castle a gagné on supprime le character*/
        int n = FindOutIndexChar(listenei, character);
        DeleteListArmy(listenei, n);
        VertexCastle *tmpc = FindCastleOfChar(character, world);
        int j = FindOutIndexChar(tmpc->castle->elements, character);
        DeleteListArmy(tmpc->castle->elements, j);
        FreeVertexArmy(character);
        return castle;
    }
    else
    { /* le character a gagné on détruit le castle */
        DeleteCastle(world, castle, character);
        world->board->matrix[character->army->coord.x][character->army->coord.y].castle = NULL;
        return NULL;
    }
}

int Fight(World *world, Coordinates coord, char choose)
{
    Case c = world->board->matrix[coord.x][coord.y];
    int a = 1;
    int b = 1;
    int test = 1;
    if (c.neighbours->size == 0)
        return 1;
    if (c.neighbours->size == 1)
    {
        b = 0;
    }
    SortListCharType(c.neighbours);
    VertexArmy *tmp1 = c.neighbours->tail;
    char coul1 = tmp1->army->color;
    VertexArmy *tmp2 = tmp1;
    while (tmp2 != NULL && tmp2->army->color == coul1)
    {
        tmp2 = tmp2->prev;
    }
    if (tmp2 == NULL)
    {
        b = 0;
    }
    while (a > 0 && b > 0)
    {
        test = 0;
        if (tmp1 == FightChar(world, tmp1, tmp2, choose))
        {
            tmp2 = c.neighbours->tail;
            while (tmp2 != NULL && tmp2->army->color == coul1)
            {
                tmp2 = tmp2->prev;
            }
            if (tmp2 == NULL)
                b = 0;
        }
        else
        {
            tmp1 = c.neighbours->tail;
            while (tmp1 != NULL && tmp1->army->color != coul1)
            {
                tmp1 = tmp1->prev;
            }
            if (tmp1 == NULL)
                a = 0;
        }
    }
    // si b==0, coul1 a gagné
    if (c.castle != NULL)
    {
        if ((b == 0 && c.castle->color != coul1) || (a == 0 && c.castle->color == coul1))
        {
            test = 0;
            VertexArmy *tmp = c.neighbours->tail;
            while (c.castle != NULL && c.neighbours->size > 0)
            {
                c.castle = FightCastle(world, tmp, c.castle, choose);
                if (c.castle != NULL)
                    tmp = tmp->prev;
            }
        }
    }
    if (world->blue->castle->head == NULL || world->red->castle->head == NULL)
        test = -2;
    return test;
}

int Mooving(World *world, Characters *army, Coordinates dest, char choose)
{
    if (dest.x == army->coord.x && dest.y == army->coord.y)
        return -1;
    army->destination = dest;
    int *graph = computeMove(world, army->coord.x, army->coord.y, army->destination.x, army->destination.y, world->turn);
    if (graph == NULL)
        return -1;
    ListDChainedArmy *list1 = world->board->matrix[army->coord.x][army->coord.y].neighbours;
    SortListCharType(list1);
    VertexArmy *character = list1->head;
    int n = 0;
    if (character->army == army)
        DeleteListArmy(list1, 0);
    else
    {
        while ((character = character->next) != NULL)
        {
            n++;
            if (character->army == army)
                break;
        }
        DeleteListArmy(list1, n);
    }
    int i = world->board->lines * character->army->coord.x + character->army->coord.y;
    character->army->task = 1;
    int xnew = graph[i] / world->board->lines;
    int ynew = graph[i] % world->board->lines;
    if (xnew == -1 || ynew == -1)
        return -1;
    character->army->coord.x = xnew;
    character->army->coord.y = ynew;
    ListDChainedArmy *list2 = world->board->matrix[xnew][ynew].neighbours;
    AddTailArmy(list2, character);
    SortListCharType(list2);
    if (xnew == character->army->destination.x && ynew == character->army->destination.y)
        army->order = '0';
    else
        army->order = 'd'; // s'il est toujours en déplacement c'est toujours son order
    army->task = '1';
    return Fight(world, army->coord, choose);
}

void MovingTurnBeginning(ListDChainedCastle *list, World *world)
{
    int n = list->size;
    VertexCastle *tmpc = list->head;
    for (int i = 0; i < n; i++)
    {
        VertexArmy *tmpa = tmpc->castle->elements->head;
        while (tmpa != NULL)
        {
            if (tmpa->army->task == '0' && tmpa->army->order == 'd')
            {
                Mooving(world, tmpa->army, tmpa->army->destination, '0');
            }
            tmpa = tmpa->next;
        }
        tmpc = tmpc->next;
    }
}

void TasksToZero(World *world)
{
    if (world->turn == world->blue->color)
    {
        VertexCastle *tmp = world->blue->castle->head;
        for (int n = 1; n <= world->blue->castle->size; n++)
        {
            VertexArmy *character = tmp->castle->elements->head;
            while (character != NULL)
            {
                character->army->task = '0';
                character = character->next;
            }
            tmp = tmp->next;
        }
    }
    else
    {
        VertexCastle *tmp = world->red->castle->head;
        VertexArmy *character = NULL;
        for (int n = 1; n <= world->red->castle->size; n++)
        {
            if (tmp->castle->elements)
                character = tmp->castle->elements->head;
            else
                return;
            while (character != NULL)
            {
                character->army->task = '0';
                character = character->next;
            }
            tmp = tmp->next;
        }
    }
}

void PeasantsProdTreasure(ListDChainedCastle *list, World *world)
{
    ListDChainedArmy *neighbours = NULL;
    int x;
    int y;
    if (world->turn == 'b')
    {
        int n = list->size;
        VertexCastle *tmpc = list->head;
        for (int i = 0; i < n; i++)
        {
            VertexArmy *tmpa = tmpc->castle->elements->head;
            while (tmpa != NULL)
            {
                if (tmpa->army->task == '0' && tmpa->army->order == 'i' && TypeToInt(tmpa->army->type) == 2)
                {
                    tmpa->army->task = '1';
                    world->blue->treasure++;
                    x = tmpa->army->coord.x;
                    y = tmpa->army->coord.y;
                    neighbours = world->board->matrix[x][y].neighbours;
                    SortListCharType(neighbours);
                }
                tmpa = tmpa->next;
            }
            tmpc = tmpc->next;
        }
    }
    else
    {
        int n = list->size;
        VertexCastle *tmpc = list->head;
        for (int i = 0; i < n; i++)
        {
            VertexArmy *tmpa = tmpc->castle->elements->head;
            while (tmpa != NULL)
            {
                if (tmpa->army->task == '0' && tmpa->army->order == 'i' && TypeToInt(tmpa->army->type) == 2)
                {
                    tmpa->army->task = '1';
                    world->red->treasure++;
                    x = tmpa->army->coord.x;
                    y = tmpa->army->coord.y;
                    neighbours = world->board->matrix[x][y].neighbours;
                    SortListCharType(neighbours);
                }
                tmpa = tmpa->next;
            }
            tmpc = tmpc->next;
        }
    }
}

void DecreeAllCastles(ListDChainedCastle *list, World *world)
{
    int n = list->size;
    VertexCastle *tmpc = list->head;
    for (int i = 0; i < n; i++)
    {
        DecreeCastle(tmpc->castle);
        tmpc = tmpc->next;
    }
}

void EndProdOfCastles(ListDChainedCastle *list, World *world)
{
    int n = list->size;
    if (n == 0)
        return;
    VertexCastle *tmp = list->head;
    for (int i = 0; i < n; i++)
    {
        if (tmp->castle->counter == 0 && tmp->castle->task != '0')
        {
            CastleBear(tmp->castle, world);
            tmp->castle->task = '0';
            tmp = tmp->next;
        }
        else
            tmp = tmp->next;
    }
}

void ChangeTurn(World *world)
{
    ListDChainedCastle *list = NULL;
    TasksToZero(world);
    if (world->turn == world->blue->color)
    {
        world->turn = 'r';
        list = world->red->castle;
    }
    else
    {
        world->turn = 'b';
        list = world->blue->castle;
    }
    MovingTurnBeginning(list, world);
    PeasantsProdTreasure(list, world);
    DecreeAllCastles(list, world);
    EndProdOfCastles(list, world);
}

int typeOfCharPlaying(World *world)
{
    ListDChainedCastle *cast = NULL;
    if (world->turn == 'b')
        cast = world->blue->castle;
    else
        cast = world->red->castle;

    if (cast == NULL)
        return -1;

    VertexCastle *tmp = cast->head;

    for (int n = 1; n <= cast->size; n++)
    {
        SortListCharType(tmp->castle->elements);
        VertexArmy *character = RemainCharPlaying(tmp->castle, 2);
        if (character != NULL && character->army != NULL)
            return 2;
        tmp = tmp->next;
    }

    tmp = cast->head;

    for (int n = 1; n <= cast->size; n++)
    {
        VertexArmy *character = RemainCharPlaying(tmp->castle, 1);
        if (character != NULL && character->army != NULL)
            return 1;
        tmp = tmp->next;
    }

    tmp = cast->head;

    for (int n = 1; n <= cast->size; n++)
    {
        VertexArmy *character = RemainCharPlaying(tmp->castle, 0);
        if (character != NULL && character->army != NULL)
            return 0;
        tmp = tmp->next;
    }

    // s'il n'y a plus de personnage à jouer on passe au prochain tour

    ChangeTurn(world);
    return -1;
}

void Suicide(World *world, VertexArmy *character)
{
    ListDChainedArmy *listevois = world->board->matrix[character->army->coord.x][character->army->coord.y].neighbours;
    int n = FindOutIndexChar(listevois, character);
    DeleteListArmy(listevois, n);
    SortListCharType(listevois);
    VertexCastle *tmpc = FindCastleOfChar(character, world);
    int j = FindOutIndexChar(tmpc->castle->elements, character);
    DeleteListArmy(tmpc->castle->elements, j);
    SortListCharType(tmpc->castle->elements);
    FreeVertexArmy(character);
}

void Immobility(World *world, VertexArmy *character)
{
    character->army->task = '1';
    character->army->order = 'i';
    character->army->destination.x = -1;
    character->army->destination.y = -1;
}

int LordToCastle(World *world, VertexArmy *lord, char color)
{
    ListDChainedCastle *list = NULL;
    VertexCastle *castle = FindCastleOfChar(lord, world);
    if (color == 'b' && world->blue->treasure >= 30)
        list = world->blue->castle;
    else if (color == 'r' && world->red->treasure >= 30)
        list = world->red->castle;
    else
        return -1;
    Coordinates coo = lord->army->coord;
    int i = coo.x;
    int j = coo.y;
    if (world->board->matrix[i][j].castle == NULL)
    {
        if (color == 'b')
            world->blue->treasure -= 30;
        else
            world->red->treasure -= 30;
        int n = 0;
        VertexArmy *tmp = castle->castle->elements->head;
        while (tmp->army != lord->army)
        {
            n++;
            tmp = tmp->next;
        }
        int j = FindOutIndexChar(world->board->matrix[coo.x][coo.y].neighbours, lord);
        DeleteListArmy(world->board->matrix[coo.x][coo.y].neighbours, j);
        DeleteListArmy(castle->castle->elements, n);
        FreeVertexArmy(lord);
        Castle *c = InitCastle(list->size, color, coo);
        VertexCastle *new = VertexInitCastle(c);
        AddTailCastle(list, new);
        world->board->matrix[coo.x][coo.y].castle = c;
        return 1;
    }
    return 0;
}
