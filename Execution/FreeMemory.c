#include "freedom.h"

void FreeCoord(Coordinates *coord)
{
    free(coord);
}

void FreeCharacter(Characters *character)
{
    free(character);
}

void FreeCastle(Castle *castle)
{
    if (castle != NULL)
    {
        free(castle->elements);
        free(castle);
    }
}

void FreeVertexArmy(VertexArmy *vertex)
{
    if (vertex != NULL)
    {
        FreeCharacter(vertex->army);
        free(vertex);
    }
}

void FreeVertexCastle(VertexCastle *vertex)
{
    FreeCastle(vertex->castle);
    free(vertex);
}

void FreeListArmy(ListDChainedArmy *list)
{
    for (VertexArmy *tmp = list->head; tmp != NULL; tmp = tmp->next)
        FreeVertexArmy(tmp->prev);
    FreeVertexArmy(list->tail);
}


void FreeCastleList(ListDChainedCastle *list)
{
    VertexCastle *tmp = list->head;
    while (tmp != NULL)
    {
        VertexCastle *tmpcop = tmp;
        tmp = tmp->next;
        FreeVertexCastle(tmpcop);
    }
    free(list);
}

void FreePairc(struct Pairc *p)
{
    FreeCoord(p->init);
    FreeCoord(p->cur);
    free(p);
}

void FreeColor(Color *color)
{
    FreeCastleList(color->castle);
    free(color);
}

void FreeCase(Case *c)
{
    FreeListArmy(c->neighbours);
    free(c);
}

void FreeBoard(Board *board)
{
    for (int i = 0; i < board->lines; i++)
        FreeCase(board->matrix[i]);

    free(board->matrix);
    free(board);
}

void FreeWorld(World *world)
{
    FreeBoard(world->board);
    FreeColor(world->red);
    FreeColor(world->blue);
    free(world);
}
