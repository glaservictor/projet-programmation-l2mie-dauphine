#ifdef __linux__
    #include<sys/ioctl.h>
    #define RED "\033[31m"
    #define BLUE "\033[34m"
    #define REST "\033[37m"
    #define PLACE(i,j) printf("\033[%d;%dH", i, j);
    #define CLEAR   printf("\e[1;1H\e[2J");
#endif

#include"Coord.h"

#ifndef MONDE_H
#include "World.h"

#ifdef __linux__ 
Coordinates* getDimTerminal()
{
    struct Coordinates* coord = malloc(sizeof(coord));
    struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);
    coord->x = w.ws_row;
    coord->y = w.ws_col;
    return coord;
}
#endif 



char** InitDisplay(World *world)
{
    size_t dChar = sizeof(world->board->rows*sizeof(char));
    char **display = malloc(world->board->lines*sizeof(dChar));
    for(int i = 0; i < world->board->lines; i++)
    {
        display[i] = malloc(sizeof(dChar));
        for(int j = 0; j < world->board->rows; j++)
            display[i][j] = ' ';
    }
    return display;
}

void  DisplayCastle(World *world,char type, char **display)
{
    VertexCastle* castle = malloc(sizeof(VertexCastle));
    if(type == 'b')
        castle = world->blue->castle->head;
    if(type == 'r')
        castle = world->red->castle->head;
    if(castle == NULL)
        return;
    display[castle->castle->coord.x][castle->castle->coord.y] = 'T';
    while(castle->next != NULL )
        {
            display[castle->castle->coord.x][castle->castle->coord.y] = 'T';
            castle = castle->next;
        }
}

void DisplayArmy(World *world, char color, char **display)
{

for(int i = 0; i < world->board->lines; i++)
    for(int j = 0; j < world->board->rows; j++)
    {
        VertexArmy *vertex = world->board->matrix[i][j].neighbours->head;
        if(vertex !=  NULL &&  vertex->army->color != color)
            continue;
        while(vertex!= NULL)
            {
                switch(vertex->army->type)
                {
                    case Lord:
                        display[i][j] = '+';
                        break;
                    case Warrior:
                        display[i][j] = '$';
                        break;
                    case Peasant:
                        display[i][j] = 'o';
                        break;
                }
                vertex = vertex->next;
            }      
    }
}

void DisplayLegend(World* world,char color)
{
    VertexArmy *vertex = NULL;
    int nbGuerrier = 0, nbSeigneur = 0, nbMan = 0;
    for(int i = 0; i < world->board->lines; i++)
    {
        for(int j = 0; j < world->board->rows; j++)
        {
            {
                vertex = world->board->matrix[i][j].neighbours->head;
                
                if( vertex != NULL && vertex->army->color == color)
                    while(vertex!=NULL)
                    {
                        switch (vertex->army->type)
                        {
                        case Lord:
                            nbSeigneur++;
                            break;
                        case Warrior:
                            nbGuerrier++;
                            break;
                        case Peasant:
                            nbMan++;
                            break;
                        default:
                            break;
                        }
                        vertex = vertex->next;
                    }
            }
        }
    }
        if(color == 'b')
        {
            #ifdef __linux__    
                printf(BLUE);
            #endif
            printf("%d Seigneurs, %d Manants, %d Warrior",nbSeigneur,nbMan,nbGuerrier);
            #ifdef __linux__    
                printf(REST);
            #endif
        }
        else
        {
            #ifdef __linux__    
                printf(RED);
            #endif 
            printf("%d Seigneurs, %d Manants, %d Warrior",nbSeigneur,nbMan,nbGuerrier);
            #ifdef __linux__    
                printf(REST);
            #endif
        }
}

void DisplayPrompt(World *world)
{
    #ifdef __linux__    
        CLEAR
    #endif
    char **displayR = InitDisplay(world);
    DisplayArmy(world,'r',displayR);   
    char **displayB = InitDisplay(world);
    DisplayArmy(world,'b',displayB);
    DisplayCastle(world,'b',displayB);
    DisplayCastle(world,'r',displayR);
    for(int i = 0; i < world->board->lines;i++)
    {
        for (int j = 0; j < world->board->rows; j++)
        {
            if(displayR[i][j] != ' ')
            {
                #ifdef __linux__    
                    printf(RED);
                #endif
                printf(" %c\t ",displayR[i][j]);
            }
            else if( displayB[i][j]!= ' ')
            {
                #ifdef __linux__    
                    printf(BLUE);
                #endif
                printf(" %c\t ",displayB[i][j]);
            }
            else 
            {
                #ifdef __linux__    
                    printf(REST);
                #endif
                printf(" .\t ");
            }
        }
    printf("\n \n\n");
    }
    DisplayLegend(world,'b');
    printf("\n");
    DisplayLegend(world,'r');
    printf("\n");
}


#endif 
