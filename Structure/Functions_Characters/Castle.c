#include<stdio.h>
#include<stdlib.h>
#include"ListChainedHeritageCastle.h"
#include"World.h"
#include"Coord.h"
#include"ListChainedHeritageCastle.h"
#include"Characters.h"
#include"listChained.h"
#include"windowsManage.h"
#include"Exec.h"



int TypeToInt(Type type){
    int val = -1;
    switch (type)
    {
    case Lord:
        val = 0;
        break;
    case Warrior:
        val = 1;
        break;
    case Peasant:
        val = 2;
        break;
    default:
        break;
    }
    return val;
}

void DecreeCastle(Castle* castle) {
    if (castle->counter > 0) { /* On ne décrémente pas un counter nul*/
        castle->counter--;
    }
}

/* Les fonctions Prod lancent la production d'un élément */


void ProdLord(Castle* castle, World* world) {
    int* treasure = malloc(sizeof(int));
    if (castle->color == 'b') treasure = &world->blue->treasure;
    else treasure = &world->red->treasure;
    
    if (castle->task == '0' && castle->counter == 0){
           if (*treasure >= 20){
                castle->task = 's';
                castle->counter = 6;
                *treasure -= 20;
           }
    } /* Il faut qu'il n'y ait plus de task en execution et suffisamment d'argent */
}


void ProdWarrior(Castle* castle, World* world) {
    int* treasure = malloc(sizeof(int));
    if (castle->color == 'b') treasure = &world->blue->treasure;
    else treasure = &world->red->treasure;
    
    if (castle->task == '0' && castle->counter == 0){
           if (*treasure >= 5){
                castle->task = 'g';
                castle->counter = 4;
                *treasure -= 5;
           }
    } /* Il faut qu'il n'y ait plus de task en execution et suffisamment d'argent */
}


void ProdPeasant(Castle* castle, World* world) {
    int *treasure = malloc(sizeof(int));
    if (castle->color == 'b')
        treasure = &world->blue->treasure;
    else
        treasure = &world->red->treasure;

    if (castle->task == '0' && castle->counter == 0)
    {
        if (*treasure >= 1)
            {
                castle->task = 'm';
                castle->counter = 2;
                *treasure -= 1;
            }
    } /* Il faut qu'il n'y ait plus de task en execution et suffisamment d'argent */
}

/* On pourrait aussi tester si le Castle est bien alive mais je ne pense pas que ce soit utile */



void CastleBear(Castle* castle, World* world) {
    if (castle->counter == 0 && castle->task !='0') { // Il que ce soit bien à la fin d'une production 
        if (castle->task == 's'){
            AddChar(castle->color, world, Lord, castle->coord,castle->coord, castle);
        }
        if (castle->task == 'g'){
            AddChar(castle->color, world, Warrior, castle->coord, castle->coord, castle);
        }
        if (castle->task == 'm'){
            AddChar(castle->color, world, Peasant, castle->coord, castle->coord, castle);
        }
    }
}

void NewDestChar(VertexArmy* character, Coordinates dest){
    if (character->army->coord.x == character->army->destination.x && character->army->coord.y == character->army->destination.y){
        character->army->destination = dest;
    } 
}
