#include"Characters.h"
#include"ListChainedHeritageCastle.h"
#include"World.h"

VertexArmy* CreateLord(struct Castle* castle, char color,int x,int y) {

    /* Création du vertex lord */
    Characters *character = malloc(sizeof(Characters));
    character->type = 0;
    character->attack = 10;
    character->coord.x = x;
    character->coord.y = y;
    character->destination.x = x;
    character->destination.y = y;
    character->color = color;
    character->task = '0';
    character->order = '0';
    VertexArmy* vertex = malloc(sizeof(VertexArmy));
    vertex->army = character;
    AddTailArmy(castle->elements,vertex);
    castle->task='0';
    return vertex;
}

VertexArmy* CreateWarrior(Castle* castle, char color,int x, int y) {

Characters *character = malloc(sizeof(Characters));
    character->type = 1;
    character->attack = 10;
    character->coord.x = x;
    character->coord.y = y;
    character->destination.x = x;
    character->destination.y = y;
    character->color = color;
    character->task = '0';
    character->order = '0';
    VertexArmy* vertex = malloc(sizeof(VertexArmy));
    vertex->army = character;
    AddTailArmy(castle->elements,vertex);
    castle->task='0';
    return vertex;
}



VertexArmy*  CreatePeasant(Castle* castle, char color,int x, int y) {

   Characters *character = malloc(sizeof(Characters));
    character->type = 2;
    character->attack = 10;
    character->coord.x = x;
    character->coord.y = y;
    character->destination.x = x;
    character->destination.y = y;
    character->color = color;
    character->task = '0';
    character->order = '0';
    VertexArmy* vertex = malloc(sizeof(VertexArmy));
    vertex->army = character;
    AddTailArmy(castle->elements,vertex);
    castle->task='0';
    return vertex;

}
