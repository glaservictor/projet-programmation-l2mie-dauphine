#include"ListChainedHeritageCastle.h"
#include"ListChainedHeritageCastle.h"
#include "Coord.h"
#include"World.h"

Castle* InitCastle(int n, char c, Coordinates coord)
{
    ListDChainedArmy *list = ListInitializationArmy();
    Castle* castle = malloc(sizeof(Castle));
    castle->attack = 0;
    castle->counter = 0;
    castle->coord = coord;
    castle->color = c;
    castle->elements = list;
    castle->number = n;
    castle->task = '0';
    castle->alive = '0';
    return castle;
}


Case *InitCase(Coordinates coord, Castle *castle,ListDChainedArmy *list)
{
    Case *cas = malloc(sizeof(Case));
    cas->castle = castle;
    cas->coord = coord;
    cas->neighbours = list;
    return cas;
}

Color* InitColor(char c)
{
    ListDChainedCastle *list = ListeInitialisationChateau();
    Color *coul = malloc(sizeof(Color));
    coul->castle = list;
    coul->turn = 0;
    coul->treasure = 50;
    coul->color = c;
    return coul;
}

Board *InitBoard(int i, int j)
{
    Board *board = malloc(sizeof(Board));
    board->lines = i;
    ListDChainedArmy *list[i][j];
    board->rows = j;
    size_t dCase = j*sizeof(Case);
    Coordinates coord;
    board->matrix = malloc(i*sizeof(dCase));
    for(int n = 0; n < i; n++)
        board->matrix[n] = malloc(j*sizeof(Case));
    for(int k = 0; k < i; k++)
        for(int n = 0; n < j;n++)
        {
            list[k][n] = ListInitializationArmy();
            coord.x = k; coord.y = n; 
            board->matrix[k][n] = *InitCase(coord,NULL,list[k][n]);
        }
    return board;
}

World* InitWorld(int i, int j)
{
    World *world = malloc(sizeof(World));
    world->blue = InitColor('b');
    world->red = InitColor('r');
    world->turn = 'b';
    world->board = InitBoard(i,j);
    return world;
}



