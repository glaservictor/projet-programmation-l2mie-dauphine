#include "ListChainedHeritageCastle.h"

ListDChainedCastle *ListeInitialisationChateau()
{
    ListDChainedCastle *list = malloc(sizeof(ListDChainedCastle));
    list->tail = NULL;
    list->head = NULL;
    list->size = 0;
    return list;
}

void equalListeArm(ListDChainedArmy *l1, ListDChainedArmy *l2)
{
    if (l1->head == NULL)
    {
        l2->head = NULL;
        l2->tail = NULL;
        return;
    }
    l2->head = l1->head;
    VertexArmy *tmp1 = l1->head;
    VertexArmy *tmp2 = l2->head;
    while ((tmp1 = tmp1->next) != NULL)
    {
        tmp2 = tmp2->next;
        tmp1->army = tmp2->army;
    }
    tmp2->next = NULL;
}

void equalCastle(Castle *C, Castle *K)
{
    if (C == NULL)
    {
        K = NULL;
        return;
    }
    K->attack = C->attack;
    K->counter = C->counter;
    K->coord.x = C->coord.y;
    K->color = C->color;
    equalListeArm(K->elements, C->elements);
    K->number = C->number;
    K->task = C->task;
    K->alive = C->alive;
}

VertexCastle *VertexInitCastle(Castle *c)
{
    VertexCastle *vertex = malloc(sizeof(VertexCastle));
    vertex->prev = NULL;
    vertex->next = NULL;
    vertex->castle = c;
    return vertex;
}

void AddTailCastle(ListDChainedCastle *list, VertexCastle *vertex)
{
    VertexCastle *tail = list->tail;
    vertex->next = NULL;
    if (list->head == NULL)
    {

        vertex->prev = NULL;
        list->head = vertex;
        list->tail = vertex;
    }
    else
    {
        tail->next = vertex;
        vertex->prev = tail;
        list->tail = vertex;
    }
    list->size++;
}

void AddHeadCastle(ListDChainedCastle *list, VertexCastle *vertex)
{
    VertexCastle *head = list->head;
    vertex->prev = NULL;
    if (list->head == NULL)
    {
        list->head = vertex;
        list->tail = vertex;
        vertex->next = NULL;
    }
    else
    {
        head->prev = vertex;
        vertex->next = head;
        list->head = vertex;
    }
    list->size++;
}

VertexCastle *DeleteListCastle(ListDChainedCastle *list, int n)
{
    if (list->head == NULL || list->tail == NULL || n >= list->size || n < 0)
        return NULL;
    VertexCastle *tmp = list->head;
    for (int i = 0; i < n; i++)
        tmp = tmp->next;
    if (tmp->prev == NULL && tmp->next == NULL)
    {
        list->tail = NULL;
        list->head = NULL;
    }
    else if (tmp->next == NULL)
    {
        list->tail = tmp->prev;
        list->tail->next = NULL;
    }
    else if (tmp->prev == NULL)
    {
        list->head = tmp->next;
        list->head->prev = NULL;
    }
    else
    {
        tmp->next->prev = tmp->prev;
        tmp->prev->next = tmp->next;
    }
    list->size--;
    return tmp;
}

ListDChainedCastle *AddListCastle(ListDChainedCastle *list, VertexCastle *vertex, int n)
{
    if (list->head == NULL && list->tail == NULL)
        AddHeadCastle(list, vertex);
    if (n > list->size)
        return NULL;
    VertexCastle *tmp = list->head;
    for (int i = 0; i < n; i++)
        tmp = tmp->next;
    vertex->prev = tmp;
    vertex->next = tmp->next;
    tmp->next->prev = vertex;
    tmp->next = vertex;
    list->size++;
    return list;
}

void DisplayListCastle(ListDChainedCastle *list)
{
    printf("NULL <-> ");
    VertexCastle *tmp = list->head;
    for (int i = 0; i < list->size; i++)
    {
        printf(" %d <-> ", tmp->castle->number);
        tmp = tmp->next;
    }
    printf(" NULL\n");
}
