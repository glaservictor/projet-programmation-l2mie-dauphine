#include"ListChainedHeritageArmy.h"

ListDChainedArmy* ListInitializationArmy()
{
    ListDChainedArmy *list = malloc(sizeof(ListDChainedArmy));
    list->tail = NULL;
    list->head = NULL; 
    list->size = 0;
    return list;
}

VertexArmy* VertexInitializationArmy(Characters *p)
{
    VertexArmy* vertex = malloc(sizeof(VertexArmy));
    vertex->army = p;
    vertex->prev = NULL;
    vertex->next = NULL;
    return vertex;
}

void AddTailArmy(ListDChainedArmy* list, VertexArmy* vertex)
{
    VertexArmy *tail = list->tail;
    vertex->next = NULL;
    if(list->head == NULL)
    {

        vertex->prev = NULL;
        list->head = vertex;
        list->tail = vertex;
    }
    else 
    {
        tail->next = vertex;
        vertex->prev = tail;
        list->tail = vertex;
    }
    list->size++;
}

void AddHeadArmy(ListDChainedArmy *list, VertexArmy *vertex)
{
    VertexArmy *head = list->head;
    vertex->prev = NULL;
    if(list->head == NULL)
    {
        list->head = vertex;
        list->tail = vertex;
        vertex->next = NULL;
    }
    else 
    {
        head->prev = vertex;
        vertex->next = head;
        list->head = vertex;
    }
    list->size++;
}

VertexArmy* DeleteListArmy(ListDChainedArmy* list, int n)
{
    if(list->head == NULL || list->tail == NULL || n>=list->size || n<0)
        return NULL;
    VertexArmy* tmp = list->head;
    for(int i = 0; i < n; i++)
        tmp = tmp->next;
    if(tmp->prev == NULL && tmp->next == NULL)
    {
        list->tail = NULL;
        list->head = NULL;
    }
    else if(tmp->next == NULL)
    {
        list->tail = tmp->prev;
        list->tail->next = NULL;
    }
    else if(tmp->prev == NULL)
    {
        list->head = tmp->next;
        list->head->prev = NULL;
    }
    else 
    {
        tmp->next->prev = tmp->prev;
        tmp->prev->next = tmp->next;

    }
    list->size--;
    return tmp;
}

ListDChainedArmy* AddListArmy(ListDChainedArmy* list, VertexArmy* vertex, int n)
{ 
    if(list->head == NULL && list->tail == NULL)
        AddHeadArmy(list,vertex);
    if(n>list->size)
        return NULL;
    VertexArmy* tmp = list->head;
    for(int i = 0; i < n; i++)
        tmp = tmp->next;
    vertex->prev = tmp;
    vertex->next = tmp->next;
    tmp->next->prev = vertex;
    tmp->next = vertex;
    list->size++;
    return list;
}


void DisplayListArmy(ListDChainedArmy *list)
{
    printf("NULL <-> ");
    VertexArmy* tmp = list->head;
    for(int i = 0; i < list->size; i++)
    {
        printf(" %d occupe : %c <-> ",tmp->army->type, tmp->army->task);
        tmp = tmp->next;
    }
    printf(" NULL\n");
}

