#include"listChained.h"
//fonctions de base

ListDChained* ListInitialization()
{
    ListDChained *list = malloc(sizeof(ListDChained));
    list->tail = NULL;
    list->head = NULL; 
    list->size = 0;
    return list;
}

Vertex* VertexInitialization(int i)
{
    Vertex* vertex = malloc(sizeof(Vertex));
    vertex->prev = NULL;
    vertex->next = NULL;
    vertex->val = i;
    return vertex;
}

void AddTail(ListDChained* list, Vertex* vertex)
{
    if(list->head == NULL)
        {

            vertex->prev = NULL;
            list->head = vertex;
            list->tail = vertex;
            vertex->next = NULL;
            printf("bjkbjk");
        }
    else 
    {
        list->tail->next = vertex;
        vertex->prev = list->tail;
        list->tail = vertex;
        vertex->next = NULL;
    }
    list->size++;

}

void AddHead(ListDChained *list, Vertex *vertex)
{
    if(list->head == NULL)
    {
        list->head = vertex;
        list->tail = vertex;
        vertex->prev = NULL;
        vertex->next = NULL;
    }
    else 
    {
        vertex->next = list->head;
        list->head = vertex;
        vertex->prev = NULL;
    }
    list->size++;

}

Vertex* DeleteList(ListDChained* list, int n)
{
    if(list->head == NULL || list->tail == NULL || n>=list->size || n<0)
        return NULL;
    Vertex* tmp = list->head;
    for(int i = 0; i < n; i++)
        tmp = tmp->next;
    if(tmp->prev == NULL && tmp->next == NULL)
    {
        list->tail = NULL;
        list->head = NULL;
    }
    else if(tmp->next == NULL)
    {
        list->tail = tmp->prev;
        list->tail->next = NULL;
    }
    else if(tmp->prev == NULL)
    {
        tmp->next->prev = NULL;
        list->head = tmp->next;
    }
    else 
    {
        tmp->prev->next = tmp->next;
        tmp->next->prev = tmp->prev;
    }
    list->size--;
    return tmp;
}

ListDChained* AddList(ListDChained* list, Vertex* vertex, int n)
{ 
    if(list->head == NULL || list->tail == NULL || n>list->size)
        return NULL;
    Vertex* tmp = list->head;
    for(int i = 0; i < n; i++)
        tmp = tmp->next;
    vertex->prev = tmp;
    vertex->next = tmp->next;
    tmp->next = vertex;
    return list;
}

void AfficheListeDRec(ListDChained *list)
{
    if(list->head == NULL)
    {
        printf(" NULL ");
        return;
    }
    printf("%d ",list->head->val);
    ListDChained tmp = *ListInitialization();
    tmp.head = list->head->next;
    printf(" %d <-> ",list->head->val );
    DisplayList(&tmp);
}

void DisplayList(ListDChained *list)
{
    printf("NULL <-> ");
    Vertex* tmp = list->head;
    for(int i = 0; i < list->size; i++)
    {
        printf(" %d <-> ",tmp->val);
        tmp = tmp->next;
    }
    printf(" NULL\n");
}
