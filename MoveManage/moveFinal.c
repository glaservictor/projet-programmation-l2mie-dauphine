#include"moveFin.h"
#include"Coord.h"


int* computeMove(World *world,int dep_x,int dep_y,int arr_x,int arr_y,char c)
{
    int size = world->board->rows, vert_arr = arr_y + world->board->rows*arr_x,vert_dep = dep_y + world->board->rows*dep_x;
    Coordinates coord = {arr_x, arr_y};
    Pair p[world->board->rows*world->board->lines*8];
    equalPairC(p,gridToGraph(world->board,c,coord),size);
    //DisplayGraph(p, size);
    int *res = dijkstra(p,vert_arr,world->board->lines);
    if(res == NULL)
        return NULL;
    Pair *graph = InitGrapheDjik(res,vert_arr,vert_dep,world->board->rows*world->board->lines);
    if(graph == NULL)
        return NULL;
    //DisplayGraph(graph,size);
    return res;
}
