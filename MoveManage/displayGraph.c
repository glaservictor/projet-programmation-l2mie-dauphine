#include"disGraph.h"
 

void my_delay(int i) {
    clock_t start,end;
    start=clock();
    while(((end=clock())-start)<=i*CLOCKS_PER_SEC);
}



int X(int i,int size)
{
    i%=size;
    i*=(WIN-BORDER)/(size-1);
    return i;
}

int Y(int i,int size) 
{
    i/=size;
    i*=(WIN-BORDER)/(size-1);
    return i;
}


Pair* InitGrapheDjik(int* gr,int beg,int end,size_t size)
{
    Pair* pair = malloc(8*size*sizeof(Pair));
    int cur = end,index = 0;
    while(cur != beg)
    {
        pair[index].x = cur;
        pair[index].y = gr[cur];
        cur = gr[cur];
        index++;
        if(index> 8*size)
            return NULL;
    }
    for(int i = index; i < 8*size;i++)
    {
        pair[i].x = 0;
        pair[i].y = 0;
    }
    return pair;
}


int DisplayGraph(Pair *pair,int size)
{
    SDL_Init(SDL_INIT_EVERYTHING);
    int x1,x2,y1,y2;
    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    SDL_Color couleurJaune = {255,255,0,255};
    SDL_SetWindowTitle(window, "Graph Looking");
    SDL_CreateWindowAndRenderer(WIN, WIN, 0, &window,&renderer);
    SDL_bool quit = SDL_FALSE;int k = 0;
    while(!quit )
    {
        SDL_Event event;
        while( SDL_PollEvent(&event))
        {
            switch (event.type)
            { 
                case SDL_KEYDOWN:
                    if(event.key.keysym.sym == SDLK_ESCAPE)
                        quit = SDL_TRUE;
            }
        }
        SDL_SetRenderDrawColor(renderer,couleurJaune.r,couleurJaune.g,couleurJaune.b,couleurJaune.a);
        for (int x = 0; x < size; x++)
        {
            x1 = X(pair[x].x,size)+BORDER/2;
            x2 = X(pair[x].y,size)+BORDER/2;
            y1 = Y(pair[x].x,size)+BORDER/2;
            y2 = Y(pair[x].y,size)+BORDER/2;
            SDL_RenderDrawLine(renderer,x1,y1,x2,y2);
        }
        k++;
        SDL_RenderPresent(renderer);
    }
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return EXIT_SUCCESS;

}
