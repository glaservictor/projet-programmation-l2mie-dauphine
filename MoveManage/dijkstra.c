#include<stdio.h>
#include"dijkstra.h"
#include<math.h>
#define INF 100

int dist[100];
int P[100];
int pred[100];


void InitPred()
{
    for(int i = 0; i < 100; i++)
    {
        P[i] = 1;
        pred[i] = -1;
    }
}

void initSommetPair(const Pair *pair,size_t size)
{
    for(int i = 0; i < 8*size; i++)
        P[pair[i].x] = 0;
}

int existeSommet(const int *P,size_t size)
{
    for(int i = 0; i < size; i ++)
        if(!P[i])
            return 1;
    return 0;
}

int sortI(const void *a,const void *b)
{
    Pair *p1 = (Pair *)a;
    Pair *p2 = (Pair *)b;
    return p1->x < p2->x;
}

int* neigh(const Pair* graph,int vertex,size_t size)
{
    int *present = malloc(8*sizeof(int));
    int index = 0;
    char* seen = calloc(size,sizeof(char));
    for(int i = 0; i < 8*size; i++)
    {
        if(graph[i].x == vertex && !seen[graph[i].y] && graph[i].y != vertex)
        {
            present[index] = graph[i].y;
            index++;
            seen[graph[i].y] = 1;
        }
    }
    for(int j = index; j < 8; j++)
        present[j] = -1;
    return present;
}

         
void predict(const Pair* graph,int cur,size_t size)
{
        int* voisin = neigh(graph,cur,size); 
        for(int j = 0; j < 8; j++)
        {
            if(!P[voisin[j]])
            {
                if(dist[voisin[j]] > dist[cur] + 1 )
                {
                    dist[voisin[j]] = dist[cur] + 1;
                    pred[voisin[j]] = cur;
                    cur = voisin[j];
                    predict(graph,cur,size);
                }
            }
        }
}



void Initialisation(const Pair* graph,int vertex,size_t size)
{
    for(int i = 0; i < size; i++)
        dist[i] = INF;
    dist[vertex] = 0;
   
}

int FindVertex(const Pair* graph,size_t size)
{
    int min = INF,vertex = -1;
    for(int i = 0; i < size; i++)
    {
        if(P[i] == 0 && dist[i] < min)
        {
            min = dist[i];
            vertex = i;
        }
    }
        return vertex;
}


void mJDistance(int a, int b, int size)
{
    if(dist[a] > dist[b] + 1)
    {
        if(a%size == b%size || a/size == b/size)
            dist[a] = dist[b] + 1;
        else 
            dist[a] = dist[b] + 2;
        pred[a] = b;
    }
}


int* dijkstra(const Pair* graph,int vertex,size_t dim)
{
    int s,index = 0,size = dim*dim;
    InitPred();  
    initSommetPair(graph,size);
    Initialisation(graph,vertex,size);
    while(existeSommet(P,size))
    {
        if(FindVertex(graph,size) == -1)
               return NULL;
        else 
            s = FindVertex(graph,size);
        P[s] = 1;
        int *voisin = neigh(graph,s,size);
        while(index<8 && voisin[index] != -1)
        {
            mJDistance(voisin[index],s,dim);
            index++;
        }
        index = 0;
        if(s==pred[s])
            return pred;
    }
    return pred;
}
