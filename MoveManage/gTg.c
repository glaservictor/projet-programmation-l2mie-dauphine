#include<stdio.h>
#include"chosePath.h"
#include<math.h>
#include"Coord.h"

#define SWITCH(S) Coordinates T = S; if(0)
#define CASE(S,_S) }else if(T.x == S && T.y == _S){switch(1) { case 1
#define BREAK }
#define DEFAULT }  else{switch(1) {case(1) 


Pair* initPair(int size)
{
    Pair* pair = malloc(8*size*size*sizeof(Pair));
    for(int i = 0; i < 8*size*size; i++)
    {
        pair[i].x = -1;
        pair[i].y = -1;
    }
    return pair;
}

void equalPairC(Pair* p1,Pair* p2,int size)
{
    for(int i = 0; i < 8*size*size; i++)
        {
            p1[i].x = p2[i].x;
            p1[i].y = p2[i].y;
        }
}


int checkUnit(Board *board, int x,int y,char c)
{
    return 1;
}

Pair* gridToGraph(Board* board, char c, Coordinates coord)
{
    int index = 0,size = board->rows;
    Pair* p = initPair(size);
    for(int i = 0; i < board->lines; i++)
        for(int j = 0; j < board->rows;j++)
        {
            if(board->matrix[i][j].neighbours->head == NULL || board->matrix[i][j].neighbours->head->army->color == c || (i == coord.x && j == coord.y))
                if (board->matrix[i][j].castle == NULL || board->matrix[i][j].castle->color == c || (i == coord.x && j == coord.y))
                {
                    if (i < size - 1 && (board->matrix[i + 1][j].neighbours->head == NULL || board->matrix[i + 1][j].neighbours->head->army->color == c || (i + 1 == coord.x && j == coord.y)))
                        if (board->matrix[i + 1][j].castle == NULL || board->matrix[i + 1][j].castle->color == c || (i + 1 == coord.x && j == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = (i + 1) * size + j;
                            ++index;
                        }
                    if (i > 0 && (board->matrix[i - 1][j].neighbours->head == NULL || board->matrix[i - 1][j].neighbours->head->army->color == c || (i - 1 == coord.x && j == coord.y)))
                        if (board->matrix[i - 1][j].castle == NULL || board->matrix[i - 1][j].castle->color == c || (i - 1 == coord.x && j == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = (i - 1) * size + j;
                            ++index;
                        }
                    if (j > 0 && (board->matrix[i][j - 1].neighbours->head == NULL || board->matrix[i][j - 1].neighbours->head->army->color == c || (i == coord.x && j - 1 == coord.y)))
                        if (board->matrix[i][j - 1].castle == NULL || board->matrix[i][j - 1].castle->color == c || (i == coord.x && j - 1 == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = i * size + j - 1;
                            ++index;
                        }
                    if (j < size - 1 && (board->matrix[i][j + 1].neighbours->head == NULL || board->matrix[i][j + 1].neighbours->head->army->color == c || (i == coord.x && j + 1 == coord.y)))
                        if (board->matrix[i][j + 1].castle == NULL || board->matrix[i][j + 1].castle->color == c || (i == coord.x && j + 1 == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = i * size + j + 1;
                            ++index;
                        }
                    if (j < size - 1 && i < size - 1 && (board->matrix[i + 1][j + 1].neighbours->head == NULL || board->matrix[i + 1][j + 1].neighbours->head->army->color == c || (i + 1 == coord.x && j + 1 == coord.y)))
                        if (board->matrix[i + 1][j + 1].castle == NULL || board->matrix[i + 1][j + 1].castle->color == c || (i + 1 == coord.x && j + 1 == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = (i + 1) * size + j + 1;
                            ++index;
                        }
                    if (j > 0 && i < size - 1 && (board->matrix[i + 1][j - 1].neighbours->head == NULL || board->matrix[i + 1][j - 1].neighbours->head->army->color == c || (i + 1 == coord.x && j - 1 == coord.y)))
                        if (board->matrix[i + 1][j - 1].castle == NULL || board->matrix[i + 1][j - 1].castle->color == c || (i + 1 == coord.x && j - 1 == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = (i + 1) * size + j - 1;
                            ++index;
                        }
                    if (j > 0 && i > 0 && (board->matrix[i - 1][j - 1].neighbours->head == NULL || board->matrix[i - 1][j - 1].neighbours->head->army->color == c || (i - 1 == coord.x && j - 1 == coord.y)))
                        if (board->matrix[i - 1][j - 1].castle == NULL || board->matrix[i - 1][j - 1].castle->color == c || (i - 1 == coord.x && j - 1 == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = (i - 1) * size + j - 1;
                            ++index;
                        }
                    if (j < size - 1 && i > 0 && (board->matrix[i - 1][j + 1].neighbours->head == NULL || board->matrix[i - 1][j + 1].neighbours->head->army->color == c || (i - 1 == coord.x && j + 1 == coord.y)))
                        if (board->matrix[i - 1][j + 1].castle == NULL || board->matrix[i - 1][j + 1].castle->color == c || (i - 1 == coord.x && j + 1 == coord.y))
                        {
                            p[index].x = i * size + j;
                            p[index].y = (i - 1) * size + j + 1;
                            ++index;
                        }
            }
        }
    return p;
}
